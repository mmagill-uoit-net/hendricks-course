#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main(int argc, char** argv) {

  // Parse input
  if (argc!=3) {
    printf("./bd_plinko.exe rseed N\n");
    return 1;
  }

  // Physical constants
  int rseed = strtol(argv[1], NULL, 10); 
  srand(rseed);
  int N     = strtol(argv[2], NULL, 10);
  float kT  = 1.0;
  float m   = 1.0;  // Unnecessary: only affects convergence time

  // Number of bins in distribution
  int Nbins = 8e1;

  // Output files (directory 'data' must exist in current directory!)
  FILE* fout_trj;
  FILE* fout_rho;
  char filename_trj[256];
  char filename_rho[256];
  sprintf(filename_trj,"data/trj_midterm.xyz");
  sprintf(filename_rho,"data/rho_midterm.dat");
  fout_trj = fopen(filename_trj,"w");
  fout_rho = fopen(filename_rho,"w");

  // Define the geometry: x in [0,10]
  float xmin =  0.0;
  float xmax = 10.0;

  // Timestep
  int Nt = 4e4;
  float dt = 0.01;

  // Physical parameters
  float zeta = 1;     // Won't affect equilibrium state, just convergence time
  float D = kT/zeta;
  float Fx = 0;

  // Allocate arrays for positions and densities
  float *x;
  x = malloc(N*sizeof(float));
  float *rho;
  rho = malloc(Nbins*sizeof(float));

  // Initialize particles and corresponding distribution (all at one point)
  for (int i=0; i<N; i++) {
    x[i] = xmin + ((xmax-xmin)/2.);
  }
  for (int i=0; i<Nbins; i++) {
    rho[i] = 0;
  }
  rho[(int) (Nbins*((xmax-xmin)/2.)/xmax)] = N;

  // Print distribution
  for (int i=0; i<Nbins; i++) {
    fprintf(fout_rho,"%f ",rho[i]);
  }
  fprintf(fout_rho,"\n");
  
  // Integrate over time
  float rnd;
  float Fext;
  for (int nt = 0; nt < Nt; nt++) {

    if ( (nt % (Nt/100))==0 ){
      printf("\r%02d%% Complete...",(100*nt)/Nt);
      fflush(stdout);
    }

    // Trajectory header
    fprintf(fout_trj,"%i\n",N);
    fprintf(fout_trj,"title\n");

    // Loop over particles
    for (int i=0; i<N; i++) {

      // Remove particle from its current bin
      rho[(int) (Nbins*(x[i]/xmax))]--;

      // Force at this position
      // Fext(x) = - grad(U(x)) = - ( -1.5*0.5*sin(1.5x) + 0.02x ) = 0.75*sin(1.5x) - 0.02x
      Fext = 0.75*sin(1.5*x[i]) - 0.02*x[i];

      // Step position (overdamped limit, BD, first-order LD)
      rnd = (float)rand()/RAND_MAX;
      rnd = rnd - 0.5;
      rnd = sqrt(12)*rnd;
      x[i] = x[i] + Fext*(dt/zeta) + sqrt(2*D*dt)*rnd;

      // Reflective BCs in x: if particle crosses a boundary, reflect it
      if ( x[i] > xmax ) {
	x[i] = xmax - (x[i] - xmax);
      }
      if ( x[i] < xmin ) {
	x[i] = xmin + (xmin - x[i]);
      }

      // Add particle to its new bin
      rho[(int) (Nbins*(x[i]/xmax))]++;
      
      // Print position to trajectory file
      fprintf(fout_trj,"a%i %f 0 0\n", i, x[i]);

    }

    // Print distribution
    for (int i=0; i<Nbins; i++) {
      fprintf(fout_rho,"%f ",rho[i]);
    }
    fprintf(fout_rho,"\n");
     
  }

  // Wrap up
  printf("\n");
  fclose(fout_rho);
  fclose(fout_trj);
  return 0;
}
