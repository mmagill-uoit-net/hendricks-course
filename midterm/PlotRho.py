import numpy as np
import matplotlib.pyplot as plt



# Extract measured distribution
#Rho_measured = np.loadtxt('data/rho_short_midterm.dat')
Rho_measured = np.loadtxt('data/rho_long_midterm.dat')


# Reconstruct x bins
N = float( np.sum(Rho_measured[-1,:]) )
Nbins = float( Rho_measured.shape[1] )
xmin = 0
xmax = 10
X = np.linspace(xmin,xmax,Nbins);


# Construct potential and Boltzmann distribution
U = 0.5*np.cos(1.5*X) + 0.01*X**2
Rho_expected = np.exp(-U) 


# Renormalize expected distribution
Rho_expected = Rho_expected * N / np.sum(Rho_expected)


# Plot
plt.plot(X,Rho_measured[-1,:],label='Measured Distribution, N=4e3, T=4e2 au')
plt.plot(X,Rho_expected,label='Boltzmann Distribution')
plt.xlabel('x')
plt.ylabel('rho(x)')
plt.legend()
plt.grid()
plt.show()


