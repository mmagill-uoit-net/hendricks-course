import numpy as np
import matplotlib.pyplot as plt
import matplotlib



# Domain size (1D)
L=100

# Load data file
A = np.loadtxt('data_rule73.dat')
tmax = A.shape[0]


# Plot
# Use -A so that colour map matches that given on the final
plt.matshow(-A[tmax-100:,:], cmap=plt.cm.hot, vmin=-1, vmax=0)
plt.xlabel('Bin number x')
plt.ylabel('Timestep t')

myfontsize=20
matplotlib.rcParams.update({'font.size': myfontsize})
plt.show()
