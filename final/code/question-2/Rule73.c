#include <stdlib.h>
#include <stdio.h>
#include <math.h>


// Lazy way to declare array size
// Set to 100 for B and C
// Set to 128 for D
#define L 100

int main() {

  // Running time
  int tmax = 1000;
  //int tmax = 100000;

  // Current and old state array
  int state[L];
  int pstate[L];

  // Output file
  FILE* fout;
  fout = fopen("data_rule73.dat","w");

  /*
  // Part b: All 1 except central cell (cell 50)
  // (there isn't really a center since we have 100 bins)
  for (int i=0; i<L; i++) {
    if(i == 50) {
      state[i]=0;
    } else {
      state[i]=1;
    }
  }  
  */

  /*
  // Part c: Random initial conditions
  srand(1);
  float rnd;
  for (int i=0; i<L; i++) {
    rnd = (float)rand()/RAND_MAX;
    if(rnd<0.5) {
      state[i]=0;
    } else {
      state[i]=1;
    }
  }
  */

  // Part d: Complicated initial conditions
  // 0- Start with interval myint at 1
  int i=0;
  int myint=1;
  while (i < L) {
    // 1- Set the next myint cells to zero
    for (int j=0; j<myint; j++) {
      state[i] = 0;
      i++;
    }

    // 2- Set the next myint cells to one
    for (int j=0; j<myint; j++) {
      state[i] = 1;
      i++;
    }
    
    // 3- Increment myint by two
    myint = myint + 2;

    // 4- Repeat from step 1
  }

  // Main loop
  int left,right; // Left and right indices
  for (int t=0; t<tmax; t++) {

    // Copy old state, print to file
    for (int i=0; i<L; i++) {
      pstate[i]=state[i];
      fprintf(fout,"%i ",state[i]);
    }
    fprintf(fout,"\n");

    // Iterate
    for (int i=0; i<L; i++) {

      // Periodic BCs
      if (i!=0) {
	left = i-1;
      } else {
	left = L-1;
      }
      if (i!=(L-1)) {
	right = i+1;
      } else {
	right = 0;
      }

      // Apply rules
      if ( pstate[i] == 1 ) {
	if ( pstate[left] && pstate[right] ) {
	  state[i] = 0;
	}
	if ( pstate[left] && !pstate[right] ) {
	  state[i] = 1;
	}
	if ( !pstate[left] && pstate[right] ) {
	  state[i] = 1;
	}
	if ( !pstate[left] && !pstate[right] ) {
	  state[i] = 0;
	}
      } else {
	if ( pstate[left] && pstate[right] ) {
	  state[i] = 0;
	}
	if ( pstate[left] && !pstate[right] ) {
	  state[i] = 0;
	}
	if ( !pstate[left] && pstate[right] ) {
	  state[i] = 0;
	}
	if ( !pstate[left] && !pstate[right] ) {
	  state[i] = 1;
	}
      }


    }


  }


  return 0;
}


