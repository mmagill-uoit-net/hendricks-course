import numpy as np
import matplotlib
import matplotlib.pyplot as plt


# Load data
FPTs_Z1 = np.loadtxt('data/times_kT100_z100_m100.dat')
FPTs_Z2 = np.loadtxt('data/times_kT100_z200_m100.dat')


# Averages
MFPT_Z1 = np.average(FPTs_Z1)
MFPT_Z2 = np.average(FPTs_Z2)


# Standard deviations
Std_Z1 = np.std(FPTs_Z1)
Std_Z2 = np.std(FPTs_Z2)


# Errors
Err_Z1 = Std_Z1/np.sqrt(1000)
Err_Z2 = Std_Z2/np.sqrt(1000)


# Print
print "1.0 & %.2f & %.2f & %.2f \\\\" % (MFPT_Z1, Std_Z1, Err_Z1)
print "2.0 & %.2f & %.2f & %.2f \\\\" % (MFPT_Z2, Std_Z2, Err_Z2)

