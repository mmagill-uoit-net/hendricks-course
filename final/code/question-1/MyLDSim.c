#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Lazy way to define number of particles
#define N 1000

int main(int argc, char** argv) {

  // WAY faster with visualization off
  int visu = 0;

  // Physical constants
  float kT   = 1.0;
  float zeta = 2.0;
  float m    = 1.0;

  // Output files
  char filename[256];
  FILE *fout;   // VMD position file
  if (visu == 1) {
    sprintf(filename,"data/xyz_kT%03d_z%03d_m%03d.xyz",(int)round(100*kT),(int)round(100*zeta),(int)round(100*m));
    fout = fopen(filename,"w");
  }
  FILE *ftimes; // First crossing times
  sprintf(filename,"data/times_kT%03d_z%03d_m%03d.dat",(int)round(100*kT),(int)round(100*zeta),(int)round(100*m));
  ftimes = fopen(filename,"w");

  // Parameters required for Velocity Verlet stepping
  float x[N];
  float vx[N];
  float ax[N];
  float vxh[N];

  // Absorption array
  int absorbed[N];
  int n_absorbed = 0;
  
  // Timestepping
  float dt = 0.01;

  // Random parameters
  float RandFromCenteredUnitUniform;
  float XiFactor = sqrt(2*kT*zeta/(m*m*dt));

  // Initial conditions
  // Start just to the right of the left boundary
  for (int i = 0; i<N; i++) {
    x[i] = 0.001;
    vx[i] = 0;
    ax[i] = 0;
    absorbed[i] = 0;
  }

  /*
  // For Boltzmann velocities (didn't have time to run this)
  for(int i=0;i<N;i++){
    r1=(float)rand()/RAND_MAX;
    r2=(float)rand()/RAND_MAX;
    gr1=sqrt(kT/(m))*sqrt(-2*log(r1))*cos(2*PI*r2);
    gr2=sqrt(kT/(m))*sqrt(-2*log(r1))*sin(2*PI*r2);
    vx[i]=gr1;
		
    ax[i]=0;
  }	
  */

  // Main loop
  int t=0;
  while ( n_absorbed < N ) {

    if ( visu == 1 ) {
      // Print VMD Header
      fprintf(fout,"%d\n",N);
      fprintf(fout,"title\n");
    }

    for ( int i=0; i<N; i++ ) {

      // For each non-absorbed particle
      if ( absorbed[i] == 0 ) {

	//// Step time using the Velocity Verlet

	// New v(t+0.5*dt)
	vxh[i] = vx[i] + 0.5*ax[i]*dt;

	// New position
	x[i] = x[i] + vxh[i]*dt;

	// Calculate the randomized acceleration 
	// Variance of centered unit uniform is 1/12
	RandFromCenteredUnitUniform = ((float)rand()/RAND_MAX)-0.5;
	ax[i] = - (zeta/m)*vxh[i] + XiFactor*RandFromCenteredUnitUniform*sqrt(12);

	// New velocity
	vx[i] = vxh[i] + 0.5*ax[i]*dt;

	// Reflecting boundary at x=0
	if ( x[i] < 0 ) {
	  x[i] = 0 - x[i];
	  vx[i] = - vx[i];
	  ax[i] = - ax[i];
	}

      }

      // Absorbing boundary at x=20
      if ( (x[i] > 20) && (absorbed[i]==0) ) {
	x[i] = 50;
	absorbed[i] = 1;
	n_absorbed++;
	fprintf(ftimes,"%f\n",t*dt);
	printf("\r%02d%% Complete...",(int) round( (float)(100*n_absorbed))/N );
	fflush(stdout);
      }

      if ( visu == 1 ) {
	// Print to file
	fprintf(fout,"AA %f 0 0\n",x[i]);
	fflush(fout);
      }

    }

    // Increment time
    t++;
  }
  printf("\r100%% Complete.  \n");

  // Close output file
  fclose(ftimes);
  if ( visu == 1 ) {
    fclose(fout);
  }

}
