\documentclass{article}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\author{Martin Magill}
\title{MCSC6280: Final}

\begin{document}

\maketitle
\thispagestyle{empty}
\newpage


\section*{Question 1}

The MFPTs were averaged over 1000 particles simulated using Langevin Dynamics for $\zeta = 1$ and $\zeta = 2$. The results are shown in the following table.
\begin{center}
\begin{tabular}{c|ccc}
$\zeta$ & MFPT & Standard Deviation & Standard Error \\
\hline
1.0 & 231.75 & 193.01 & 6.10 \\
2.0 & 418.67 & 347.58 & 10.99 \\
\end{tabular}
\end{center}

From the discussion concerning Arrhenius processes in the lecture notes, the timescale required to go from any point $a$ to any point $b$ is a potential is given by
\begin{align*}
\tau = \frac{1}{D} \int_a^b e^{-U(y)/kT} \int_{-\infty}^y e^{-U(x)/kT} dx dy.
\end{align*}

In this case, the potential is effectively
\begin{align*}
U(x) = \left\{\begin{array}{cc}
\infty & x<0 \\
0 & x>0 \\
\end{array} \right.,
\end{align*}
so using $D = kT/\zeta$ for a single particle in LD and $kT=1$, we can approximate the MFPT as
\begin{align*}
\tau &= \zeta\int_0^{20} \int_0^y dx dy, \\
 &= \zeta\int_0^{20} y dy, \\
 &= \zeta \frac{y^2}{2} \bigg|_0^{20} , \\
 &= 200 \zeta.
\end{align*}

This is in remarkably good agreement with the simulated results. At $\zeta = 2.0$ the computed MFPT is within 2 standard errors of the theoretical result. At $\zeta = 1.0$, the agreement is still good, but lies 5 standard errors above the theoretical value. The measured MFPTs do agree very well with a linear relation between the MFPT and $\zeta$.

The decreased accuracy at lower $\zeta$ may be attributable to an increased contribution from inertia. The Arrhenius formulation for MFPT assumes a purely diffusive transport process. As $\zeta$ decreases, the inertial ballistic motion of particles on short timescales becomes increasingly important. However, since ballistic motion transports particles faster than diffusive motion, one would expect the error to be in the opposite direction. 

This confusing behaviour may be attributable to the initial conditions of the particles, which are in this case initially stationary. At short timescales, then, the particles do not diffuse. This produces a systematic increase of first passage times until the temperature equilibrates. Since the total first passage times at larger $\zeta$ are much longer, this transient error is less significant in that case. (I don't have time to re-run with initial velocities from a Boltzmann distribution, but I've added the code in a comment).



\newpage
\section*{Question 2}

\subsection*{Part b}

The evolution over 100 iterations of Rule 73 for the first set of initial conditions are shown in Figure~\ref{fig:q2b}. All figures in this section use the same colour code as the final exam, namely cells in state 1 are black and cells in state 0 are white.

\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth]{plots/q2b.eps}
\caption{The evolution under Rule 73 of initial conditions where all cells start in state 1 except the central cell. \label{fig:q2b}}
\end{center}
\end{figure}

Due to the periodic boundary conditions, the initial state can be regarded as a single stretch of 99 cells in state 1 separated by a single cell in state 0 from another stretch of 99 ones. Infinite stretches of cells of identical type will oscillate between the two states forever, so the action of the system is centered at the cell initialized in cell 0.

The perturbation from the center cell propagates outwards in a stable fashion for the first 50 timestep. The cells far from the center cell oscillate between states 1 and 0 with a period of 1 until the wave reaches them. The wave creates an array of stable oscillators of period 3, which oscillate through
\begin{enumerate}
\item 0111
\item 0101
\item 0000
\item 0111
\end{enumerate}
An array of these configuration is stable forever as long as each oscillator is offset in its phase by 1 iteration from its neighbours.

In this simulation, the initial wave eventually replaces almost the entire domain with these stable 3-oscillators. When the wave reaches the boundaries, it interferes with itself. Beyond timestep 50, a second wave propagates inwards from the boundary points.

This secondary wave replaces the stable array of 3-oscillators with a more complicated configuration of structures. Larger contiguous stretches of an odd number of cells in state 1 form. These tend to decay in width by 2 cells every 3 cycles, becoming smaller, self-similar structures until they return to 3-oscillator states.

Since this configuration is not a perfect array of 3-oscillators, these structures are not stable for very long. New contiguous neighbourhoods of cells is state 1 form and decay back into 3-oscillators. The behaviour is difficult to predict, and possible chaotic.


\subsection*{Part c}

The evolution of the randomly initialized domain is shown in Figure~\ref{fig:q2c}. Several structures appear.

\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth]{plots/q2c.eps}
\caption{The evolution under Rule 73 of initial conditions where each cell is randomly initialized in either binary state. \label{fig:q2c}}
\end{center}
\end{figure}

The most striking structures in Figure~\ref{fig:q2c} are the large, stable bands. These consist of 2 cells in the same state bordered on either side by cells in the opposite state. Figure~\ref{fig:q2c} contains 13 bands with cells in state 1 bordered by cells in state 0 and one band of cells in state 0 bounded by cells in state 1. The black bands are provably stable, as follows:
\begin{enumerate}
\item Rules 2 and 5 (on the final handout) ensure that a black band is stable as long as the cells to its left and right remain white. Rules 4 and 7 ensure the converse for white bands.

\item Rules 3 and 4 ensure that a white cell to the right of a black band will remain white regardless of its other neighbour. Rules 3 and 7 ensure the same thing for the white cell to the left of a black band.
\end{enumerate}

White bands are not stable in general. For instance, rule 6 allows the bounding black cell of a white band to change to white if its other neighbour is white. The white band in Figure~\ref{fig:q2c}, however, must be stable, since it is bounded by two black bands, each of which is stable.

Figure~\ref{fig:q2c} also demonstrates that the 3-oscillators described in Part b are stable when located between two black bands.

Another stable structure emerges in Figure~\ref{fig:q2c}, as seen between the 3rd and 4th, 4th and 5th, and 13th and 1st black bands. These regions are filled with oscillators of period 2. There must be an odd number of cells between the neighbouring black bands. The cells neighbouring the black bands remain white, as do all cells an even number away from those. The other cells in the region oscillate between 0 and 1. Neighbouring switching cells are out of phase by 1 iteration.

Finally, Figure~\ref{fig:q2c} demonstrates two more complex structures. Between the 9th and 10th black bands there is a structure of period 20. Since it repeats exactly 4 times and is bounded by the black bands (which are proven to be stable), this 20-oscillator is itself stable.

Between the 2nd and 3rd black bands, a structure of even larger period arises. It also repeats exactly during the first 100 timesteps, and since it is bounded by black bands it must also be stable.

Running the same simulation for longer confirms that these structures are indeed stable. Running with different random seeds still tends to produce several stable black bands. Between black bands that are not very far apart, the solutions seem to always be periodic. The farther between the black bands, the less trivial it is to establish whether the confined region is periodic at all, or if sufficiently large regions between black bands can lead to chaotic solutions. This behaviour merits additional exploration.


\subsection*{Part d}

The evolution of the final choice of initial conditions is shown in Figure~\ref{fig:q2d}.

\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth]{plots/q2d.eps}
\caption{The evolution under Rule 73 of the specific set of initial conditions described on the final and in the text. The domain is extended to $L=128$ for this set. \label{fig:q2d}}
\end{center}
\end{figure}

The choice of initial conditions in Figure~\ref{fig:q2d} seems to assure that the black bands seen in Part c cannot form. No periodic behaviour is evident.

The decaying structures seen in the wake of the secondary wave from Part b arise and decay throughout the domain in Figure~\ref{fig:q2d}. The solution tends to decay to the 2-oscillator and 3-oscillator solutions described previously. However, the lack of structure means that these oscillators are not stable. 

This solution may very well be chaotic. Even after running for 100,000 iterations, the solution looks essentially the same as that in Figure~\ref{fig:q2d}.











































































\end{document}

 