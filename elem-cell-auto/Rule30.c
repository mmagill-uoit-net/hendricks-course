#include <stdlib.h>
#include <stdio.h>
#include <math.h>


// Lazy way to declare array size
#define L 100

int main() {

  // Running time
  int tmax = 100;

  // Current and old state array
  int state[L];
  int pstate[L];

  // Output file
  FILE* fout;
  fout = fopen("data_rule30.dat","w");

  // Initial conditions
  float rnd;
  for (int i=0; i<L; i++) {
    rnd = (float)rand()/RAND_MAX;
    if(rnd<0.5) {
      state[i]=0;
    } else {
      state[i]=0;
    }
  }
  state[L/2] = 1;


  // Main loop
  int left,right; // Left and right indices
  for (int t=0; t<tmax; t++) {

    // Copy old state, print to file
    for (int i=0; i<L; i++) {
      pstate[i]=state[i];
      fprintf(fout,"%i ",state[i]);
    }
    fprintf(fout,"\n");

    // Iterate
    for (int i=0; i<L; i++) {

      // Periodic BCs
      if (i!=0) {
	left = i-1;
      } else {
	left = L-1;
      }
      if (i!=(L-1)) {
	right = i+1;
      } else {
	right = 0;
      }

      // Apply rules
      if ( pstate[i] == 1 ) {
	if ( pstate[left] && pstate[right] ) {
	  state[i] = 0;
	}
	if ( pstate[left] && !pstate[right] ) {
	  state[i] = 0;
	}
	if ( !pstate[left] && pstate[right] ) {
	  state[i] = 1;
	}
	if ( !pstate[left] && !pstate[right] ) {
	  state[i] = 1;
	}
      } else {
	if ( pstate[left] && pstate[right] ) {
	  state[i] = 0;
	}
	if ( pstate[left] && !pstate[right] ) {
	  state[i] = 1;
	}
	if ( !pstate[left] && pstate[right] ) {
	  state[i] = 1;
	}
	if ( !pstate[left] && !pstate[right] ) {
	  state[i] = 0;
	}
      }


    }


  }


  return 0;
}


