#!/bin/bash



for kT in 0 0.001 0.01 0.1 0.2 0.3 0.4 0.5
do
    echo $kT

	./bd_plinko.exe 1 $kT 0 >> data/PTs_NoObsts_$kT.dat
	./bd_plinko.exe 1 $kT 1 >> data/PTs_Obsts_$kT.dat

done

