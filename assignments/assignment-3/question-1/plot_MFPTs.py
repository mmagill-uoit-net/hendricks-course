import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import glob 


kTs = [0,0.001,0.01,0.1,0.2,0.3,0.4,0.5]

mean_FPTs_NoObsts = np.zeros(len(kTs))
mean_FPTs_Obsts = np.zeros(len(kTs))
sdev_FPTs_NoObsts = np.zeros(len(kTs))
sdev_FPTs_Obsts = np.zeros(len(kTs))

for i in range(len(kTs)):

    fname_NoObsts = "data/PTs_NoObsts_%s.dat" % kTs[i]
    fname_Obsts = "data/PTs_Obsts_%s.dat" % kTs[i]

    FPTs_NoObsts = np.loadtxt(fname_NoObsts)
    FPTs_Obsts = np.loadtxt(fname_Obsts)

    mean_FPTs_NoObsts[i] = np.mean(FPTs_NoObsts)
    mean_FPTs_Obsts[i] = np.mean(FPTs_Obsts)

    sdev_FPTs_NoObsts[i] = np.std(FPTs_NoObsts)
    sdev_FPTs_Obsts[i] = np.std(FPTs_Obsts)

#    plt.plot(kTs[i]*np.ones(FPTs_NoObsts.shape),FPTs_NoObsts,'bo')
#    plt.plot(kTs[i]*np.ones(FPTs_Obsts.shape),FPTs_Obsts,'ro')


kTs = np.array(kTs)
fig, axarr = plt.subplots(1,2)

axarr[0].errorbar(kTs,mean_FPTs_NoObsts,yerr=sdev_FPTs_NoObsts/np.sqrt(100),color='b',label="No Obstacles")
axarr[0].errorbar(kTs,mean_FPTs_Obsts,yerr=sdev_FPTs_Obsts/np.sqrt(100),color='r',label="Obstacles")
axarr[0].grid()
axarr[0].set_xlim(-0.05,0.55)
axarr[0].set_xlabel("kT")
axarr[0].set_ylabel("MFPTs w/ Std Err")
axarr[0].legend(loc='upper left')

axarr[1].plot(kTs,mean_FPTs_Obsts-mean_FPTs_NoObsts,color='g')
axarr[1].plot(kTs,50*kTs**1,'k--')
axarr[1].text(0.04,0.4,r'$(kT)^{\alpha},\, \alpha=1$')
axarr[1].grid()
axarr[1].set_xlim(5e-4,1)
axarr[1].set_xlabel("kT")
axarr[1].set_ylabel("Difference in MFTP")
axarr[1].set_xscale("log")
axarr[1].set_yscale("log")

fig.subplots_adjust(left=0.08,bottom=0.14,right=0.95,
                    top=0.9,wspace=0.29,hspace=0.2)
matplotlib.rcParams.update({'font.size': 20})
plt.show()


