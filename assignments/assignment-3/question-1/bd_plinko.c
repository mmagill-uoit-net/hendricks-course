#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main(int argc, char** argv) {

  // Parse input
  if (argc!=4) {
    printf("./bd_plinko.exe rseed kT obst_flag\n");
    return 1;
  }

  // Physical constants
  int rseed = strtol(argv[1], NULL,10); 
  srand(rseed);
  float kT   = strtof(argv[2], NULL);
  int obst_flag = strtol(argv[3], NULL,10);

  // Output file
  FILE* fout_trj;
  char filename[256];
  sprintf(filename,"data/plinko_trj_%04d_%02d_%d.xyz",rseed,(int)round(10*kT),obst_flag);
  fout_trj = fopen(filename,"w");

  // Define the geometry
  float L = 50;
  float spacing = 2.5;
  int N = 100;
  int Nobst_PerRow = round(L/spacing);
  int Nobst = pow(Nobst_PerRow,2.);

  // Timestep
  int Nt = 1e6;
  float dt = 0.005;

  // Physical parameters
  float zeta = 1;
  float D = kT/zeta;
  float Fx = 0;
  float Fy = -1;

  // WCA parameters
  float eps = 1;
  float sigma = 1;
  float rc = pow(2.0,1.0/6.0)*sigma;

  // Allocate arrays for particles and obstacles
  float *x;
  float *y;
  float *xobst;
  float *yobst;
  x = malloc(N*sizeof(float));
  y = malloc(N*sizeof(float));
  xobst = malloc(Nobst*sizeof(float));
  yobst = malloc(Nobst*sizeof(float));

  // Initialize particle (all at one point)
  for (int i=0; i<N; i++) {
    x[i] = L/2;
    y[i] = L+5;
  }

  // Set obstacle positions in a square array
  if (obst_flag == 1) {
    for (int i=0; i<Nobst_PerRow; i++) {
      for (int j=0; j<Nobst_PerRow; j++) {
	xobst[j+i*Nobst_PerRow] = (i+0.5) * spacing;
	yobst[j+i*Nobst_PerRow] = (j+0.5) * spacing;
      }
    }
  }

  // Integrate over time
  float rnd;
  float r;
  float Fwca;
  float Fwcax;
  float Fwcay;
  int npassed = 0;
  float *passed = malloc(N*sizeof(float));
  int passage_flag = 0;
  for (int nt = 0; nt < Nt; nt++) {

    /*
    if ( (nt % (Nt/100))==0 ){
      printf("\r%02d%% of time limit has elapsed...",(100*nt)/Nt);
      fflush(stdout);
    }
    */

    // Trajectory header
    fprintf(fout_trj,"%i\n",N+Nobst);
    fprintf(fout_trj,"title\n");

    // Loop over particles
    for (int i =0; i<N; i++) {

      // Reset obstacle forces
      Fwca=0;
      Fwcax=0;
      Fwcay=0;

      // Detect obstacle collisions
      if ( obst_flag == 1 ) {
	for (int k=0; k<Nobst; k++) {
	  r = sqrt(pow(x[i]-xobst[k],2.0)+pow(y[i]-yobst[k],2.0));
	  if (r<rc) {
	    Fwca = -4*eps*( -12.0*pow(sigma,12.0)/pow(r,13.0) + 6.0*pow(sigma,6.0)/pow(r,7.0) );
	    Fwcax = Fwca * (x[i]-xobst[k])/r;
	    Fwcay = Fwca * (y[i]-yobst[k])/r;
	  }
	}
      }

      // Step freely
      rnd = (float)rand()/RAND_MAX;
      rnd = rnd - 0.5;
      rnd = sqrt(12)*rnd;
      x[i] = x[i] + ((Fx+Fwcax)/zeta)*dt + sqrt(2*D*dt)*rnd;

      // Periodic BCs in x
      if ( x[i] > L ) {
	x[i] = x[i]-L;
      }
      if ( x[i] < 0 ) {
	x[i] = x[i]+L;
      }

      rnd = (float)rand()/RAND_MAX;
      rnd = rnd - 0.5;
      rnd = sqrt(12)*rnd;
      y[i] = y[i] + ((Fy+Fwcay)/zeta)*dt + sqrt(2*D*dt)*rnd;

      if ( (y[i] < 0) && (passed[i]==0) ) {
	passed[i] = 1;
	printf("%f\n",nt*dt);
	npassed++;
	if ( npassed == N ) {
	  passage_flag = 1;
	}
      }

      fprintf(fout_trj,"a%i %f %f 0\n", i, x[i], y[i]);
    }

    // Obstacle positions
    for (int i=0; i<Nobst_PerRow; i++) {
      for (int j=0; j<Nobst_PerRow; j++) {
	fprintf(fout_trj,"a%i %f %f 0\n",N+j+i*Nobst_PerRow,xobst[j+i*Nobst_PerRow],yobst[j+i*Nobst_PerRow]);
      }
    }
    
    // Check if all particles have passed the post
    if ( passage_flag == 1 ) {
      nt = Nt;
    }

    
  }

  return 0;
}
