import numpy as np
import matplotlib.pyplot as plt




def PlotMSDxyz( kT, zeta, m ):

    filename_suffix = 'kT%03d_z%03d_m%03d' % (100*kT,100*zeta,100*m)
#    msdx = np.loadtxt('overlap/msdx_%s.dat' % filename_suffix);
#    msdy = np.loadtxt('overlap/msdy_%s.dat' % filename_suffix);
#    msdz = np.loadtxt('overlap/msdz_%s.dat' % filename_suffix);
    msdx = np.loadtxt('data/msdx_%s.dat' % filename_suffix);
    msdy = np.loadtxt('data/msdy_%s.dat' % filename_suffix);
    msdz = np.loadtxt('data/msdz_%s.dat' % filename_suffix);
    
    times = (np.arange(len(msdx))+1) * 0.01
    
    # Get x,y,z values of correlation length and D
    fig, axarr = plt.subplots(2,1)
    
    axarr[0].plot(times,msdx[:,1],'r')
    axarr[0].plot(times,msdy[:,1],'g')
    axarr[0].plot(times,msdz[:,1],'b')
    axarr[0].set_xlabel('Window Width in Units of Time')
    axarr[0].set_ylabel('MSD')
    axarr[0].set_ylim(ymin=0)
    
    # Labels
    axarr[0].text(80,100,'MSD$_x$',color='r',fontsize=20)
    axarr[0].text(80,70,'MSD$_y$',color='g',fontsize=20)
    axarr[0].text(80,40,'MSD$_z$',color='b',fontsize=20)
    axarr[0].text(40,40,'Expected MSD',color='k',fontsize=20)
    
    # Theoretical result
    D = kT/zeta
    axarr[0].plot(times,2*D*times,'k--',linewidth=3);
    axarr[1].plot(times,2*D*times,'k--',linewidth=3);


    ## Log-log plot
    
    # Basic plot
    axarr[1].set_xscale('log')
    axarr[1].set_yscale('log')
    axarr[1].plot(times,msdx[:,1],'r')
    axarr[1].plot(times,msdy[:,1],'g')
    axarr[1].plot(times,msdz[:,1],'b')
    axarr[1].set_xlabel('Window Width in Units of Time')
    axarr[1].set_ylabel('MSD')
    
    # Correlation length
    corr_len = 700
    times_ballistic = times[:corr_len]
    times_diffusive = times[70:]
    axarr[1].plot(times_ballistic,times_ballistic**2,'k--')
    axarr[1].plot(times_diffusive,2*times_diffusive,'k--')
    axarr[1].axvline(7.,color='k',linestyle='--')
    axarr[1].text(.8,5.e-2,'Ballistic motion')
    axarr[1].text(8.,2.e2,'Diffusive motion')
    axarr[1].text(8.,5.e-2,'Correlation length \n~ %d timesteps' % corr_len)
    
    
    ## Fit lines for D
    # <dx^2>/t = 2dD
    # Fit is best for short-ish times, ~3 corr_len
    px = np.polyfit(times[corr_len:3*corr_len],msdx[corr_len:3*corr_len,1],1)
    py = np.polyfit(times[corr_len:3*corr_len],msdy[corr_len:3*corr_len,1],1)
    pz = np.polyfit(times[corr_len:3*corr_len],msdz[corr_len:3*corr_len,1],1)    
    axarr[0].plot(times,times*px[0]+px[1],'r--')
    axarr[0].plot(times,times*py[0]+py[1],'g--')
    axarr[0].plot(times,times*pz[0]+pz[1],'b--')
    axarr[0].text(20,200,r'$D_x \approx %.2f$' % (px[0]/2),fontsize=20)
    axarr[0].text(20,170,r'$D_y \approx %.2f$' % (py[0]/2),fontsize=20)
    axarr[0].text(20,140,r'$D_z \approx %.2f$' % (pz[0]/2),fontsize=20)
    
    plt.show()

    return (px[0]/2.,py[0]/2.,pz[0]/2.)




def PlotMSDr( kT, zeta, m ):

    filename_suffix = 'kT%03d_z%03d_m%03d' % (100*kT,100*zeta,100*m)
#    msdr = np.loadtxt('overlap/msdr_%s.dat' % filename_suffix);
    msdr = np.loadtxt('data/msdr_%s.dat' % filename_suffix);
    
    times = (np.arange(len(msdr))+1) * 0.01
    
    # Get r values of correlation length and D
    fig, axarr = plt.subplots(2,1)

    axarr[0].plot(times,msdr[:,1],'b')
    axarr[0].set_xlabel('Window Width in Units of Time')
    axarr[0].set_ylabel('MSD')
    axarr[0].set_ylim(ymin=0)
    
    # Labels
    axarr[0].text(70,600,'MSD$_r$',color='b',fontsize=20)
    axarr[0].text(65,300,'Expected MSD',color='k',fontsize=20)
    
    # Theoretical result
    D = kT/zeta
    axarr[0].plot(times,6*D*times,'k--',linewidth=3);
    axarr[1].plot(times,6*D*times,'k--',linewidth=3);


    ## Log-log plot
    
    # Basic plot
    axarr[1].set_xscale('log')
    axarr[1].set_yscale('log')
    axarr[1].plot(times,msdr[:,1],'b')
    axarr[1].set_xlabel('Window Width in Units of Time')
    axarr[1].set_ylabel('MSD')
    
    # Correlation length
    corr_len = 700
    times_ballistic = times[:corr_len]
    times_diffusive = times[70:]
    axarr[1].plot(times_ballistic,3*times_ballistic**2,'k--')
    axarr[1].plot(times_diffusive,6*times_diffusive,'k--')
    axarr[1].axvline(7.,color='k',linestyle='--')
    axarr[1].text(.8,5.e-2,'Ballistic motion')
    axarr[1].text(8.,5.e-2,'Diffusive motion')
    axarr[1].text(8.,5.e-4,'Correlation length \n~ %d timesteps' % corr_len)
    
    
    ## Fit lines for D
    # <dx^2>/t = 2dD
    # Fit is best for short-ish times, ~3 corr_len
    pr = np.polyfit(times[corr_len:3*corr_len],msdr[corr_len:3*corr_len,1],1)    
    axarr[0].plot(times,times*pr[0]+pr[1],'b--')    
    axarr[0].text(20,500,r'$D_r \approx %.2f$' % (pr[0]/6),fontsize=20)
    
    plt.show()

    return pr[0]/6.



def PlotMSDxyz_VarykT( kTs ):
    
    # List of Ds
    Dxs = np.zeros(len(kTs))
    Dys = np.zeros(len(kTs))
    Dzs = np.zeros(len(kTs))
    Drs = np.zeros(len(kTs))

    # Plot D_x, D_y, D_z against zeta
    zeta = 1
    m = 1
    for i in range(len(kTs)):

        kT = kTs[i]
        Dx,Dy,Dz = PlotMSDxyz(kT,zeta,m)
        Dr = PlotMSDr(kT,zeta,m)

        Dxs[i]=Dx
        Dys[i]=Dy
        Dzs[i]=Dz
        Drs[i]=Dr


    plt.plot(kTs,Dxs,'r')
    plt.plot(kTs,Dys,'g')
    plt.plot(kTs,Dzs,'b')
    plt.plot(kTs,Drs,'m')
    plt.plot(kTs,Dxs,'ro')
    plt.plot(kTs,Dys,'go')
    plt.plot(kTs,Dzs,'bo')
    plt.plot(kTs,Drs,'mo')
    plt.plot(kTs,kTs,'k--')
    plt.xlabel('Thermal Energy, $kT$')
    plt.ylabel('Diffusion Coefficient, $D$')
    plt.text(2e-2,10**(0.5),'$D_x$',fontsize=20,color='r')
    plt.text(2e-2,10**(0.3),'$D_y$',fontsize=20,color='g')
    plt.text(2e-2,10**(0.1),'$D_z$',fontsize=20,color='b')
    plt.text(2e-2,10**(-0.1),'$D_r$',fontsize=20,color='m')
    plt.text(2e-2,10**(-0.3),r'$D = kT/\zeta$',fontsize=20,color='k')
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(ymin=5e-3)
    plt.show()


def PlotMSDxyz_VaryZeta( zetas ):
    
    # List of Ds
    Dxs = np.zeros(len(zetas))
    Dys = np.zeros(len(zetas))
    Dzs = np.zeros(len(zetas))
    Drs = np.zeros(len(zetas))

    # Plot D_x, D_y, D_z against zeta
    kT = 1
    m = 1
    for i in range(len(zetas)):

        zeta = zetas[i]
        Dx,Dy,Dz = PlotMSDxyz(kT,zeta,m)
        Dr = PlotMSDr(kT,zeta,m)

        Dxs[i]=Dx
        Dys[i]=Dy
        Dzs[i]=Dz
        Drs[i]=Dr


    plt.plot(zetas,Dxs,'r')
    plt.plot(zetas,Dys,'g')
    plt.plot(zetas,Dzs,'b')
    plt.plot(zetas,Drs,'m')
    plt.plot(zetas,Dxs,'ro')
    plt.plot(zetas,Dys,'go')
    plt.plot(zetas,Dzs,'bo')
    plt.plot(zetas,Drs,'mo')
    plt.plot(zetas,kT/np.array(zetas),'k--')
    plt.xlabel(r'Friction Coefficient, $\zeta$')
    plt.ylabel('Diffusion Coefficient, $D$')
    plt.text(5,10**(0.5),'$D_x$',fontsize=20,color='r')
    plt.text(5,10**(0.3),'$D_y$',fontsize=20,color='g')
    plt.text(5,10**(0.1),'$D_z$',fontsize=20,color='b')
    plt.text(5,10**(-0.1),'$D_r$',fontsize=20,color='m')
    plt.text(5,10**(-0.3),r'$D = kT/\zeta$',fontsize=20,color='k')
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(ymin=5e-3)
    plt.show()



def PlotMSDxyz_VaryM( ms ):
    
    # List of Ds
    Dxs = np.zeros(len(ms))
    Dys = np.zeros(len(ms))
    Dzs = np.zeros(len(ms))
    Drs = np.zeros(len(ms))

    # Plot D_x, D_y, D_z against zeta
    kT = 1
    zeta = 1
    for i in range(len(ms)):

        m = ms[i]
        Dx,Dy,Dz = PlotMSDxyz(kT,zeta,m)
        Dr = PlotMSDr(kT,zeta,m)

        Dxs[i]=Dx
        Dys[i]=Dy
        Dzs[i]=Dz
        Drs[i]=Dr


    plt.plot(ms,Dxs,'r')
    plt.plot(ms,Dys,'g')
    plt.plot(ms,Dzs,'b')
    plt.plot(ms,Drs,'m')
    plt.plot(ms,Dxs,'ro')
    plt.plot(ms,Dys,'go')
    plt.plot(ms,Dzs,'bo')
    plt.plot(ms,Drs,'mo')
    plt.axhline(kT/zeta,color='k')
    plt.xlabel('Mass, $m$')
    plt.ylabel('Diffusion Coefficient, $D$')
    plt.text(2e-2,10**(0.7),'$D_x$',fontsize=20,color='r')
    plt.text(2e-2,10**(0.6),'$D_y$',fontsize=20,color='g')
    plt.text(2e-2,10**(0.5),'$D_z$',fontsize=20,color='b')
    plt.text(2e-2,10**(0.4),'$D_m$',fontsize=20,color='m')
    plt.text(2e-2,10**(0.3),r'$D = kT/\zeta$',fontsize=20,color='k')
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(ymin=5e-3)
    plt.show()


