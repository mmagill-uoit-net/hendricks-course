#!/bin/bash


zeta=1
m=1
for kT in 0.01 0.05 0.10 0.5 1.0 5.0
do
    ./MyLDSim.exe $kT $zeta $m
done

kT=1
m=1
for zeta in 0.01 0.05 0.10 0.5 1.0 5.0
do
    ./MyLDSim.exe $kT $zeta $m
done

kT=1
zeta=1
for m in 0.01 0.05 0.10 0.5 1.0 5.0
do
    ./MyLDSim.exe $kT $zeta $m
done



