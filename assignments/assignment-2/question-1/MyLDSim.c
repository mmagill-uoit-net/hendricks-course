#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char** argv) {

  // Parse input
  if (argc!=4) {
    printf("./MyLDSim.exe kT zeta m\n");
    return 1;
  }

  // Plotting On/Off
  int plotting = 0;

  // Physical constants
  float kT   = strtof(argv[1], NULL);
  float zeta = strtof(argv[2], NULL);
  float m    = strtof(argv[3], NULL);

  // Output file
  FILE *fout;
  char filename[256];
  sprintf(filename,"data/xyz_kT%03d_z%03d_m%03d.dat",(int)round(100*kT),(int)round(100*zeta),(int)round(100*m));
  fout = fopen(filename,"w");

  // Parameters required for Velocity Verlet stepping
  float x,y,z, vx,vy,vz, ax,ay,az, vxh,vyh,vzh;
  
  // Timestepping
  float dt = 0.01;
  int Nt = 1000000;
  int imax = (int) round(Nt/dt);

  // Random parameters
  float RandFromCenteredUnitUniform;
  float XiFactor = sqrt(2*kT*zeta/(m*m*dt));

  // Initial conditions
  x = 0;   y = 0;  z = 0;
  vx = 0; vy = 0; vz = 0;
  ax = 0; ay = 0; az = 0; 

  // Main loop
  for ( int i=0; i<Nt; i++ ) {

    //// Step time using the Velocity Verlet

    // New v(t+0.5*dt)
    vxh = vx + 0.5*ax*dt;
    vyh = vy + 0.5*ay*dt;
    vzh = vz + 0.5*az*dt;

    // New position
    x = x + vxh*dt;
    y = y + vyh*dt;
    z = z + vzh*dt;

    // Calculate the randomized acceleration 
    // Variance of centered unit uniform is 1/12
    RandFromCenteredUnitUniform = ((float)rand()/RAND_MAX)-0.5;
    ax = - (zeta/m)*vxh + XiFactor*RandFromCenteredUnitUniform*sqrt(12);
    RandFromCenteredUnitUniform = ((float)rand()/RAND_MAX)-0.5;
    ay = - (zeta/m)*vyh + XiFactor*RandFromCenteredUnitUniform*sqrt(12);
    RandFromCenteredUnitUniform = ((float)rand()/RAND_MAX)-0.5;
    az = - (zeta/m)*vzh + XiFactor*RandFromCenteredUnitUniform*sqrt(12);

    // New velocity
    vx = vxh + 0.5*ax*dt;
    vy = vyh + 0.5*ay*dt;
    vz = vzh + 0.5*az*dt;

    // Print to file
    fprintf(fout,"%f %f %f\n",x,y,z);
    fflush(fout);

    // Plot
    if ( (i % (Nt/100))==0 ){
      printf("\r%02d%% Complete...",(100*i)/Nt);
      fflush(stdout);
    }

  }
  printf("\r100%% Complete.  \n");

  // Plot
  if (plotting==1) {
    printf("Plotting...\n");
    FILE *pout;
    pout = popen("gnuplot -persist > /dev/null 2>&1","w");
    fprintf(pout,"set terminal x11 1\n");
    fprintf(pout,"set key off\n");
    fprintf(pout,"set xrange[-100:100]\n");
    fprintf(pout,"set yrange[-100:100]\n");
    fprintf(pout,"set zrange[-100:100]\n");
    fprintf(pout, "splot 'dat_MyLDSim_xyz.dat' with lines using 1:2:3 \n");
    fflush(pout);
    printf("Done.\n");
    fclose(pout);
  }

  // Close output file
  fclose(fout);

}
