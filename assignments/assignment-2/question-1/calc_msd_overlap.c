#include <stdio.h>
#include <stdlib.h>
#include <math.h>



int main(int argc, char** argv) {

  // Parse input
  if (argc!=4) {
    printf("./calc_msd.exe kT zeta m\n");
    return 1;
  }

  // Physical constants
  float kT   = strtof(argv[1], NULL);
  float zeta = strtof(argv[2], NULL);
  float m    = strtof(argv[3], NULL);

  // Files
  FILE* fin;
  FILE* foutx;
  FILE* fouty;
  FILE* foutz;
  FILE* foutr;
  char filenamein[256];
  char filenamex[256];
  char filenamey[256];
  char filenamez[256];
  char filenamer[256];
  sprintf(filenamein,"data/xyz_kT%03d_z%03d_m%03d.dat",(int)round(100*kT),(int)round(100*zeta),(int)round(100*m));
  sprintf(filenamex,"data/msdx_kT%03d_z%03d_m%03d.dat",(int)round(100*kT),(int)round(100*zeta),(int)round(100*m));
  sprintf(filenamey,"data/msdy_kT%03d_z%03d_m%03d.dat",(int)round(100*kT),(int)round(100*zeta),(int)round(100*m));
  sprintf(filenamez,"data/msdz_kT%03d_z%03d_m%03d.dat",(int)round(100*kT),(int)round(100*zeta),(int)round(100*m));
  sprintf(filenamer,"data/msdr_kT%03d_z%03d_m%03d.dat",(int)round(100*kT),(int)round(100*zeta),(int)round(100*m));
  fin = fopen(filenamein,"r");
  foutx = fopen(filenamex,"w");
  fouty = fopen(filenamey,"w");
  foutz = fopen(filenamez,"w");
  foutr = fopen(filenamer,"w");


  // Line count in fin
  int dummy;
  int N = 0;
  while (!feof(fin)) {
    dummy = fgetc(fin);
    if ( dummy == '\n' ) {
      N++;
    }
  }
  rewind(fin);


  // Read the input file
  printf("Reading input file...");
  float* x = malloc(N*sizeof(float));
  float* y = malloc(N*sizeof(float));
  float* z = malloc(N*sizeof(float));
  float readx,ready,readz;
  for (int i=0; i<N; i++) {
    fscanf(fin,"%f %f %f",&readx,&ready,&readz);
    if(!feof(fin)){
      x[i]=readx;
      y[i]=ready;
      z[i]=readz;
    }
  }
  printf("\tDone.\n");


  // Analyse the array
  // Window sizes are j*dt for each j
  // Windows range from pos1 to pos2
  // For a given window size, nwindows is the number that were found
  int pos1,pos2;
  int nwindows;
  float dx,dy,dz;
  float dx2,dy2,dz2;
  float sum_dx,sum_dy,sum_dz;
  float avg_dx,avg_dy,avg_dz,avg_dr;
  float sum_dx2,sum_dy2,sum_dz2;
  float avg_dx2,avg_dy2,avg_dz2,avg_dr2;
  int max_width = N/100;
  for ( int width_in_bins=1; width_in_bins<max_width; width_in_bins++ ) {

    if ( (width_in_bins%100)==0 ) {
      printf("\r%02d%% Complete...",(100*width_in_bins)/max_width);
      fflush(stdout);
    }

    // Start with window at the start
    pos1 = 0;
    pos2 = pos1 + width_in_bins;

    // Compute average of dx and (dx)^2 over every [pos1,pos1+width_in_bins]
    nwindows = 0;
    sum_dx = 0; sum_dy = 0; sum_dz = 0;
    sum_dx2 = 0; sum_dy2 = 0; sum_dz2 = 0;
    while ( pos2 < N ) {

      // Compute deltas
      dx = x[pos2] - x[pos1];
      dy = y[pos2] - y[pos1];
      dz = z[pos2] - z[pos1];

      // Compute squares of deltas
      dx2 = dx*dx;
      dy2 = dy*dy;
      dz2 = dz*dz;

      // Add this window
      nwindows++;
      sum_dx += dx;
      sum_dy += dy;
      sum_dz += dz;
      sum_dx2 += dx2;
      sum_dy2 += dy2;
      sum_dz2 += dz2;

      // Increment to next window (overlapping windows)
      pos1++;
      pos2 = pos1 + width_in_bins;
    }
    avg_dx =  sum_dx  / nwindows;
    avg_dy =  sum_dy  / nwindows;
    avg_dz =  sum_dz  / nwindows;
    avg_dr =  pow( avg_dx*avg_dx + avg_dy*avg_dy + avg_dz*avg_dz, 0.5);
    avg_dx2 = sum_dx2 / nwindows;
    avg_dy2 = sum_dy2 / nwindows;
    avg_dz2 = sum_dz2 / nwindows;
    avg_dr2 = avg_dx2 + avg_dy2 + avg_dz2;

    // Print to file
    fprintf(foutx,"%d %e\n",width_in_bins,avg_dx2-avg_dx*avg_dx);
    fprintf(fouty,"%d %e\n",width_in_bins,avg_dy2-avg_dy*avg_dy);
    fprintf(foutz,"%d %e\n",width_in_bins,avg_dz2-avg_dz*avg_dz);
    fprintf(foutr,"%d %e\n",width_in_bins,avg_dr2-avg_dr*avg_dr);

  }
  printf("\nDone.\n");


}
