import numpy as np
import matplotlib.pyplot as plt



RhoE = np.loadtxt('enrg_radiation.dat')


plt.plot(RhoE[:,0],RhoE[:,1])


plt.yscale('log')
plt.xlim(0,2500)
plt.xlabel('Depth, AU')
plt.ylabel('Energy Deposited, AU')
plt.grid()
plt.show()


