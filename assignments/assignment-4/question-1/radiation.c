#include "stdlib.h"
#include "stdio.h"
#include "math.h"

int main(int argc, char** argv) {

  // Trajectory file
  FILE *ftraj = fopen("traj_radiation.xyz","w");
  FILE *fenrg = fopen("enrg_radiation.dat","w");

  // Simulation parameters
  float m = 1.0;
  float dt = 1.0;
  float t = 0.;
  float tmax = 1000.;

  // Collision parameters
  float rnd;
  float p = 0.05;
  float Elost = 0.2;
  float Ethresh = 0.1;

  // Particle variables
  float v;
  int N = 1e5;
  float *x = malloc(N*sizeof(float));
  float *y = malloc(N*sizeof(float));
  float *E = malloc(N*sizeof(float));

  // Declare energy histogram
  int Nbins = 1000;
  float *rhoE = malloc(Nbins*sizeof(float));
  float width = 5.0;

  //
  
  // Initialize
  // Particles start at x=0, with a meaningless offset in y
  // Initial energies uniformly distributed between 4 and 5
  fprintf(ftraj,"%i\n",N);
  fprintf(ftraj,"title\n");
  for (int i = 0; i < N; i++) {
    x[i] = 0;
    y[i] = i*0.1;
    fprintf(ftraj,"a%i %f %f 0\n",i,x[i],y[i]);
    E[i] = 4.0 + (float)rand()/RAND_MAX;
  }
  for (int i = 0; i < Nbins; i++) {
    rhoE[i] = 0;    
  }
  
  // At each timestep,
  float Ndead;
  while ( (t < tmax) && (Ndead<N) ) {

    // Update status
    if ( ((int)t % ((int)tmax/100))==0 ){
      printf("\r%02d%% Complete...",(int)((100*t)/tmax));
      fflush(stdout);
    }


    // Print the VMD header
    fprintf(ftraj,"%i\n",N);
    fprintf(ftraj,"title\n");

    // For each particle,
    Ndead = 0;
    for (int i = 0; i < N; i++) {

      // If its energy is above the threshold,
      if ( E[i] > Ethresh ) {
	// Either
	rnd = (float)rand()/RAND_MAX;

	// the particle experiences a collision,
	if ( rnd < p ) {
	  E[i] -= Elost;
	  rhoE[(int)floor(x[i]/width)] += Elost;   /// BUG!: If x[i] is large enough this can seg fault
	} // if COLLISION
	// or it moves forward
	else { 
	  v = sqrt( (2./m) * E[i] );
	  x[i] += v*dt;
	}

      }//if TRESHOLD
      // otherwise it is dead.
      else {
	Ndead++;
      }

      // Print the particle's position
      fprintf(ftraj,"a%i %f %f 0\n",i,x[i],y[i]);

    }//for PARTICLES

    t += dt;
  }//while TIME

  // Print the final energy histogram
  for( int i = 0; i < Nbins; i++) {
    fprintf(fenrg,"%f %f\n",i*width,rhoE[i]);
  }

  // Complete
  fclose(ftraj);
  fclose(fenrg);
  return 0;
}
