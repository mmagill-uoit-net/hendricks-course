\documentclass{article}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\author{Martin Magill}
\title{MCSC6280: Assignment 1}

\begin{document}

\maketitle
\thispagestyle{empty}
\newpage


\section*{Question 1}

\subsection*{Part a}

The original function is in fact not a probability distribution function, as it is not normalized to have an integral of 1 over the given domain. Since

\begin{align*}
\int_0^\pi \sin(x') \,dx' = 2,
\end{align*}

we must renormalize, so that the PDF is

\begin{align*}
\rho(x) = \frac{1}{2} \sin(x), \quad x \in [0,\pi].
\end{align*}

This PDF is shown in Figure~\ref{fig:Q1_PDFandCDF}.

\begin{figure}[p]
\begin{center}
\includegraphics[width=0.9\textwidth]{plots/A1Q1_PDFandCDF.eps}
\caption{The PDF and CDF for question 1. \label{fig:Q1_PDFandCDF}}
\end{center}
\end{figure}

Because this is a simple PDF, we can easily find the CDF and inverse CDF by hand. The CDF, also shown in Figure~\ref{fig:Q1_PDFandCDF} is given by

\begin{align*}
P(x) &= \int_0^x \rho(x') \,dx' \\
 &= \frac{1}{2} \int_0^x \sin(x') \,dx', \\
 &= -\frac{1}{2} \cos(x') \bigg|_0^x, \\
 &= - \frac{1}{2} (\cos(x) - 1), \\
 &= \frac{1}{2} (1 - \cos(x)).
\end{align*}

This goes from 0 to 1 as $x$ goes from 0 to $\pi$, as expected. Given a number $y$ drawn from the unit uniform distribution, we can obtain $x$ drawn from $\rho(x)$ by solving

\begin{align*}
y &= \frac{1}{2} (1 - \cos(x)), \\
\cos(x) &= 1 - 2y, \\
x &= \arccos( 1 - 2y ).
\end{align*}

In other words, the inverse CDF is

\begin{align*}
P^{-1}(y) = \arccos( 1 - 2y ).
\end{align*}


\subsection*{Part b}

The desired PDF can be sampled by applying $P^{-1}$ to the output of the standard C function \verb-rand()-. Figure~\ref{fig:Q1_data} details the results of doing this repeatedly. 

The left plot compares the expected counts in 100 equal bins across $[0,\pi]$ when 10, 25, 50, 75, and 100 thousand numbers were sampled. The relative variation about the expected distribution appears to increase as the number of samples increase. In fact, the standard deviation in the number of counts $N$ in each bin is roughly $\sqrt{N}$.

As such, renormalizing the numerical distributions by the total number of samples creates a histogram where the standard deviation of the fractional occupation of each bin is roughly $1/\sqrt{N}$. Thus the normalized numerical distributions converge to the theoretical PDF as the number of samples increases. This is shown in the two right plots: the top plots shows the distrbution of 10 thousand samples, whereas the bottom shows the distribution of 100 thousand samples. Clearly the bottom plot is a much better approximation of the theoretical expected distribution (shown as the dotted black line).

\subsubsection*{Codes and Data}

The numbers sampled from $\rho(x)$ are stored in the files called \verb-Nsamples_[N].dat-. These are produced in C using \verb|part-a.exe|, which can be recompiled using \verb|make|. The data is binned and plotted in Python using \verb|part-b.py|.


\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{plots/A1Q1_data.eps}
\caption{Histograms of numbers sampled from $\rho(x)$ using inverse transform sampling. Bins indicate numerical data, whereas dotted lines indicate theoretical expected values. \label{fig:Q1_data}}
\end{center}
\end{figure}


\newpage
\section*{Question 2}


\subsection*{Part a}

Let's use a polynomial distribution, since it makes integration easy. I'll choose it of the following form:

\begin{align*}
\rho(x) = \left\{ \begin{array}{cc}
a x^3 + b x^2 + c x + f, & x \in [-1,2] \\
0, & \text{else}
\end{array} \right. .
\end{align*}

First we normalize:

\begin{align*}
1 := \int_{-1}^2 \rho(x) \,dx &= \int_{-1}^2 \left( a x^3 + b x^2 + c x + f \right) \,dx, \\
&= \left( \frac{a}{4} x^4 + \frac{b}{3} x^3 + \frac{c}{2} x^2 + f x \right)\bigg|_{-1}^2 ,\\
&= \left( \frac{a}{4} (16 - 1) + \frac{b}{3} (8 + 1) + \frac{c}{2} (4 - 1) + f (2 + 1) \right), \\
\therefore 1  &= \frac{15}{4} a + 3 b + \frac{3}{2} c + 3 f, \\
4 &= 15 a + 12 b + 6 c + 12 f.
\end{align*}

Now we compute the mean:

\begin{align*}
0 := \int_{-1}^2 x \rho(x) \,dx &= \int_{-1}^2 \left( a x^4 + b x^3 + c x^2 + f x \right) \,dx, \\
&= \left( \frac{a}{5} x^5 + \frac{b}{4} x^4 + \frac{c}{3} x^3 + \frac{f}{2} x^2 \right)\bigg|_{-1}^2 ,\\
&= \left( \frac{a}{5} (32 + 1) + \frac{b}{4} (16 - 1) + \frac{c}{3} (8 + 1) + \frac{f}{2} (4 - 1) \right) ,\\
\therefore 0 &= \frac{33}{5} a + \frac{15}{4} b + 3 c + \frac{3}{2} f, \\
0 &= 132 a + 75 b + 60 c + 30 f .
\end{align*}

Finally, we compute the standard deviation:

\begin{align*}
1 := \int_{-1}^2 x^2 \rho(x) \,dx &= \int_{-1}^2 \left( a x^5 + b x^4 + c x^3 + f x^2 \right) \,dx, \\
&= \left( \frac{a}{6} x^6 + \frac{b}{5} x^5 + \frac{c}{4} x^4 + \frac{f}{3} x^3 \right)\bigg|_{-1}^2 ,\\
&= \left( \frac{a}{6} (64 - 1) + \frac{b}{5} (32 + 1) + \frac{c}{4} (16 - 1) + \frac{f}{3} (8 + 1) \right) ,\\
\therefore 1 &= \frac{63}{6} a + \frac{33}{5} b + \frac{15}{4} c + 3 f,\\
30 &= 315 a + 198 b + \frac{225}{2} c + 90 f ,\\
60 &= 630 a + 396 b + 225 c + 180 f.
\end{align*}

In matrix form, this is

\begin{align*}
\left[ \begin{array}{cccc}
15 & 12 & 6 & 12 \\
132 & 75 & 60 & 30 \\
630 & 396 & 225 & 180 
\end{array} \right] \left[ \begin{array}{c}
a \\ b \\ c \\ f
\end{array} \right] = \left[ \begin{array}{c}
4 \\ 0 \\ 60
\end{array} \right]
\end{align*}

Using Octave to solve the system yields:

\begin{align*}
\text{rref}(A|b) = \left[ \begin{array}{cccc|c}
1 & 0 & 0 & -20/11 & -140/297 \\
0 & 1 & 0 &  30/11 &  320/297 \\
0 & 0 & 1 &  12/11 &  -92/297
\end{array} \right]
\end{align*}

such that

\begin{align*}
\left[ \begin{array}{c}
a \\ b \\ c \\ f
\end{array} \right] = \frac{1}{297} \left[ \begin{array}{c}
-140 \\ 320 \\ -92 \\ 0
\end{array} \right] + \frac{1}{11} \left[ \begin{array}{c}
20 \\ -30 \\ -12 \\ 11
\end{array} \right] s
\end{align*}

for a free parameter $s$.

So our distribution would be

\begin{align*}
\rho(x;s) = \frac{1}{297} \left( -140x^3 + 320x^2 -92x \right )+ s \frac{1}{11} \left(20x^3 - 30x^2 - 12x + 11 \right)
\end{align*}

on $x \in [-1,2]$, and zero elsewhere. We have one remaining degree of freedom, $s$, which we will use to enforce the non-negativity of the solution everywhere.

The global minimum can be located either at the boundaries, $x=-1$ or $x=2$, or at the zeros of the derivative. We have

\begin{align*}
\rho(-1;s) &= \frac{1}{297} \left( 140 + 320 + 92 \right )+ s \frac{1}{11} \left(-20 - 30 + 12 + 11 \right) \\
 &= \frac{552}{297} - s \frac{27}{11},
\end{align*}

which imposes 

\begin{align*}
6072 - 8019 s &> 0, \\
s < \frac{6072}{8019} &= \frac{184}{243}.
\end{align*}

Next we have

\begin{align*}
\rho(2;s) &= \frac{1}{297} \left( -1120 + 1280 - 184 \right )+ s \frac{1}{11} \left(160 - 120 - 24 + 11 \right) \\
 &= -\frac{24}{297} + s \frac{27}{11} ,
\end{align*}

which imposes

\begin{align*}
-\frac{24}{297} + s \frac{27}{11} &> 0, \\
s \frac{27}{11} &> \frac{24}{297}, \\
s > \frac{8}{243}.
\end{align*}

So for values of $s$ satisfying

\begin{align*}
\frac{8}{243} < s < \frac{184}{243}
\end{align*}

the distribution evaluated at the bounds will be non-negative. Finally, we must consider interior minima. Conveniently, however, trying $s=8/243$ yields a solution with no negative values in the interior of the domain, so we can simply proceed with this value. [This trial was conducted in WolframAlpha.]

So, finally, the distribution we will use for this question is

\begin{align*}
\rho(x) &= \frac{1}{297} \left( -140x^3 + 320x^2 -92x \right )+ \frac{8}{243} \frac{1}{11} \left(20x^3 - 30x^2 - 12x + 11 \right), \\
 &= \frac{1}{81} \frac{9}{297} \left[ 9 \left( -140x^3 + 320x^2 -92x \right )+ 8 \left(20x^3 - 30x^2 - 12x + 11 \right) \right], \\
 &= \frac{1}{81} \frac{9}{297} \left( -1100 x^3 + 2640 x^2 - 924 x + 88 \right), \\
 &= \frac{1}{243} \left( -100x^3 + 240x^2 - 84 x + 8 \right).
\end{align*}

Again, WolframAlpha confirms this has all the desired quantities, so this is indeed a viable PDF for the problem.


\subsection*{Part b}

Since we used inverse transform sampling in Part a, let's use rejection sampling for this problem.

We can re-use much of the code from the previous section, simply adding a method to sample using this different technique. We need to use the maximum of the distribution. WolframAlpha reveals that the maximum value of the PDF on our domain is

\begin{align*}
y_{\max} = \frac{16}{9}.
\end{align*}

Figure~\ref{fig:Q2_samples} demonstrates that the code is indeed sampling the distribution correctly.


\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{plots/A1Q2_samples.eps}
\caption{Histogram of numbers sampled from the cubic distribution using rejection sampling. Bins indicate numerical data, whereas the dotted line indicates theoretical expected values. A total of 100 thousand samples were drawn for this plot. \label{fig:Q2_samples}}
\end{center}
\end{figure}

The central limit theorem states that the measured average of these distributions will itself be a random number. This average will be distributed according to

\begin{align*}
\phi(x) = \frac{1}{\sqrt{2\pi \sigma}} e^{- \frac{x^2}{2\sigma^2}}
\end{align*}

with

\begin{align*}
\sigma = \frac{1}{\sqrt{N_{\text{Samples}}}},
\end{align*}

since the cubic distribution has unit standard deviation. Figure~\ref{fig:Q2_averages} shows the distribution of 1000 averages measured from 100 thousand samples drawn from the cubic distribution. The dotted black line shows the distribution predicted by the central limit theorem. The agreement is quite good, as expected.

\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{plots/A1Q2_averages.eps}
\caption{Histogram of the average of 100 thousand samples from the cubic distribution obtained using rejection sampling. Bins indicate numerical data, whereas the dotted line indicates the Gaussian distribution predicted by the central limit theorem. A total of a thousand distributions were averaged for this plot. \label{fig:Q2_averages}}
\end{center}
\end{figure}



\newpage
\section*{Question 3}

Figure~\ref{fig:Q3_PVrelation} shows the predicted and measured pressure-volume relation for the simulated ideal gas in a box. The simulation was conducted for 1000 helium atoms at 300K. The components of particle velocities were reversed when the particles crossed walls of the box.

The timestep was taken as 5\% of the time needed for an atom moving with $\frac{1}{2} m v_i^2 = \frac{1}{2} kT$ to cross the width of the box. Particles were initialized with velocities drawn from a Maxwell-Boltzmann distribution, which was sampled using Python's \verb-random.gauss- function. 

\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth]{plots/A1Q3_PVrelation.eps}
\caption{The pressure-volume relation for an ideal gas. The dotted line indicates the expected theoretical values for 1000 helium atoms at 300K, whereas the circles represent time-averaged values obtained from the simulation. \label{fig:Q3_PVrelation}}
\end{center}
\end{figure}














\end{document}

 