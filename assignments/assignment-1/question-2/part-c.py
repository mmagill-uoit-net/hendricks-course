import numpy as np
import matplotlib.pyplot as plt


import matplotlib
matplotlib.rcParams.update({'font.size': 20})


def CalcHistogram(filename):

    ## These are constants for the current problem
    #xmin = -1
    #xmax = 2

    # Now we're plotting the averages, whose SD should be 1/sqrt(Nsamples) ~ 0.00316
    xmin = -0.01
    xmax =  0.01

    # Nbins is 20% of Nsamples
    Nsamples = int(filename.split("_")[1].split(".")[0])
    Nbins = 100

    with open(filename,'r') as fin:

        data = np.loadtxt(fin)
        hist = np.histogram(data, bins=np.linspace(xmin,xmax,Nbins))

    return hist


def PlotHistogram(hist):

    Range = hist[1][-1] - hist[1][0]
    Nbins = hist[1].shape[0]
    TheWidth = Range / Nbins
    plt.bar(hist[1][:-1],hist[0],width=TheWidth)
#    plt.xlim([-1,2])
    plt.xlim([-0.01,0.01])
#    plt.show()



def PlotByNs(Nsamples,Naverages):
    
    casename = 'Nsamples_%06d_Naverages_%06d.dat' % (Nsamples,Naverages)
    hist = CalcHistogram(casename)
    PlotHistogram(hist)



def PlotPDF(Nsamples):

    x = np.linspace(-1,2,100)
    dx = x[1] - x[0]
    y = (1./243.)*( -100*x**3 + 240*x**2 - 84*x + 8 )
    y = y * dx * Nsamples
    plt.plot(x,y,'k--',linewidth=5)


def PlotGaussian(Nsamples,Naverages):

    sigma = 1/np.sqrt(Nsamples)

    x = np.linspace(-0.01,0.01,100)
    dx = x[1] - x[0]
    y = (1./np.sqrt(2*np.pi*sigma**2))*np.exp(- x**2/(2.*sigma**2) )
    y = y * dx * Naverages
    plt.plot(x,y,'k--',linewidth=5)
    


# # Make plot to prove distribution is being sampled
# PlotByNsamples(100000,'b')
# PlotPDF(100000)
# plt.xlabel('x')
# plt.ylabel('Nsamples')
# plt.show()


# Make plot of averages
PlotByNs(100000,1000)
PlotGaussian(100000,1000)
plt.xlabel('Sample Mean')
plt.ylabel('Frequency')
plt.show()


