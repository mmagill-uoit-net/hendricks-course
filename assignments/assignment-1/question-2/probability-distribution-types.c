#include "probability-distribution-types.h"



void InitializeDistribution( Distribution* dist,
			     PDF pdf, CDF cdf, ICDF icdf,
			     double xmin, double xmax, double ymax ) {

  // Range over which the distribution is defined
  dist->xmin = xmin;
  dist->xmax = xmax;

  // Vertical max of the distribution (for rejection sampling)
  dist->ymax = ymax;

  // PDF, CDF, and iCDF
  dist->pdf = pdf;
  dist->cdf = cdf;
  dist->icdf = icdf;

}


double SampleDistributionUsingITS( Distribution* dist ) {

  // Sample the unit uniform PDF
  double SampledFrom_UnitUniform = (double) rand() / RAND_MAX;

  // Invert the CDF
  double SampledFrom_Distribution = dist->icdf(SampledFrom_UnitUniform);

  // Return the new sample
  return SampledFrom_Distribution;

}



double SampleDistributionUsingRejection( Distribution* dist ) {

  // Continue until sampled
  double SampledFrom_Distribution;
  int Rejected = 1;
  while (Rejected==1) {

    // Sample the unit uniform PDF
    double SampledFrom_UnitUniform = (double) rand() / RAND_MAX;
    
    // Scale to range
    double SampledFrom_UniformOnRange =
      (dist->xmax - dist->xmin) * SampledFrom_UnitUniform + dist->xmin;

    // Rejection probability
    double PdfEvaluatedHere = dist->pdf(SampledFrom_UniformOnRange);
    double RejectionRoll = (double) dist->ymax * rand() / RAND_MAX;
    if ( RejectionRoll < PdfEvaluatedHere ) {
      SampledFrom_Distribution = SampledFrom_UniformOnRange;
      Rejected = 0;
    }
  }


  // Return the new sample
  return SampledFrom_Distribution;

}








