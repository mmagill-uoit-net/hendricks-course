#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "probability-distribution-types.h"
#include "custom-cubic-distribution.h"



int main (int argc, char* argv[]) {
  // Take in two command line argument, which must be Nsamples and Naverages
  if ( argc != 3 ) {
    printf("./part-a Nsamples Naverages\n");
    return 1;
  }
  char* stopped;
  long Nsamples = strtol(argv[1], &stopped, 10);
  long Naverages = strtol(argv[2], &stopped, 10);
  if (*stopped || Nsamples <= 0 || Naverages <= 0) {
    printf("Ns must be positive integers!");
    return 1;
  }

  // Make the chosen distribution
  double xmin = -1;
  double xmax = 2;
  double ymax = 16./9.;
  PDF pdf = &CustomCubic_PDF;
  CDF cdf = &CustomCubic_PDF; // Dummy 
  ICDF icdf = &CustomCubic_PDF; // Dummy
  Distribution* dist = malloc(sizeof(Distribution));
  InitializeDistribution(dist,pdf,cdf,icdf,xmin,xmax,ymax);

  // Compute the mean of Naverages distributions, each containing Nsamples samples
  double* averages = malloc(Naverages * sizeof(double));
  double sum, average;
  for ( int j=0; j < Naverages; j++ ) {

    // Average the samples
    sum = 0;
    for ( int i=0; i < Nsamples; i++ ) {
      sum += SampleDistributionUsingRejection(dist);
    }
    average = sum/Nsamples;
    averages[j] = average;

  }

  // Print averages to file
  char foutname[256];
  sprintf(foutname,"Nsamples_%06lu_Naverages_%06lu.dat",Nsamples,Naverages);
  FILE* fout = fopen(foutname,"w");
  for ( int i=0; i < Naverages; i++ ) {
    fprintf(fout,"%.6f\n",averages[i]);
  }
  fclose(fout);
  
  // Wrap up
  free(averages);
  return 0;
}
