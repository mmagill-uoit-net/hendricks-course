#include "custom-cubic-distribution.h"



double CustomCubic_PDF( double x ) {

  if ( x < -1 ) {
    return 0;
  } 
  else if ( x > 2 ) {
    return 0;
  }
  else {
    return (1./243.)*( -100*x*x*x + 240*x*x - 84*x + 8 );
  }

}


