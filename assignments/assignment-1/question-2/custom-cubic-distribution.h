#ifndef __CUSTOM_CUBIC_DISTRIBUTION_H__
#define __CUSTOM_CUBIC_DISTRIBUTION_H__



#include <math.h>

// Only PDF is needed for rejection sampling
double CustomCubic_PDF( double x );



#endif /* __CUSTOM_CUBIC_DISTRIBUTION_H__ */


