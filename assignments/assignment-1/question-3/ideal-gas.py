import numpy as np
import matplotlib.pyplot as plt
import random

import matplotlib
matplotlib.rcParams.update({'font.size': 20})


import MyConstants
reload(MyConstants)
from MyConstants import *

import Type_IdealGasParticle
reload(Type_IdealGasParticle)
from Type_IdealGasParticle import *

###
#
# Simulate an ideal gas in a box
# All quantities handled in SI units
#
###

def SimulateIdealGas(N,V,T):

    ## Simulation conditions

    # Box dimensions (goes from -L to L in all dimensions)
    L = 0.5*V**(1./3.)
    TotalArea = 6.*(2*L)**2

    # Particle mass (kg)
    m = 6.64648e-27  # Helium

    # Time variables
    dt = L / np.sqrt(k_B*T/m)
    dt = dt / 20.
    Nsteps = 200.
    

    ## Initialize particles

    # Array of particles
    Particles = np.ndarray((N,),IdealGasParticle)

    # Assign positions uniformly, velocities from Boltzmann
    for particle in Particles:

        particle['x'] = random.uniform(-L,L)
        particle['y'] = random.uniform(-L,L)
        particle['z'] = random.uniform(-L,L)

        particle['vx'] = DrawFrom_BoltzmannVelocityPDF(m=m,T=T)
        particle['vy'] = DrawFrom_BoltzmannVelocityPDF(m=m,T=T)
        particle['vz'] = DrawFrom_BoltzmannVelocityPDF(m=m,T=T)


    ## Propagate particles, measuring pressure
    Pressures = np.ndarray((Nsteps,),dtype='float64')
    Nt = 0
    while ( Nt < Nsteps ):
        
        TransferredMomentum = 0
        for particle in Particles:
            
            # If near a wall, reverse direction and count a collision
            if ( abs(particle['x'])>L ) :
                particle['vx'] = - particle['vx']
                TransferredMomentum += 2*m*abs(particle['vx'])
            if ( abs(particle['y'])>L ) :
                particle['vy'] = - particle['vy']
                TransferredMomentum += 2*m*abs(particle['vy'])
            if ( abs(particle['z'])>L ) :
                particle['vz'] = - particle['vz']
                TransferredMomentum += 2*m*abs(particle['vz'])
                
            # Otherwise, just keep moving
            particle['x'] += dt*particle['vx']
            particle['y'] += dt*particle['vy']
            particle['z'] += dt*particle['vz']
            
        # Measure pressure
        ForceOnWalls = TransferredMomentum / dt
        Pressure = ForceOnWalls / TotalArea
        Pressures[Nt] = Pressure
        
        # Increment
        Nt += 1

    AveragePressure = np.mean(Pressures)
    return AveragePressure


# Measure P-V relation, compare to ideal gas
N = 1000
T = 300.
Vs = np.logspace(-5,1,30)
PVpairs = np.ndarray((len(Vs),2))
for i in range(len(Vs)):

    V = Vs[i]
    print "Doing V = %.1e" % V

    P = SimulateIdealGas(N,V,T)

    PVpairs[i,0] = V
    PVpairs[i,1] = P


Ps_ideal = N*k_B*T / Vs
plt.plot(PVpairs[:,0],PVpairs[:,1],'o')
plt.plot(PVpairs[:,0],Ps_ideal,'k--')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Volume, cubic meters')
plt.ylabel('Pressure, Pascals')
plt.title('Measured vs Ideal Gas, N=1000, T=300K')
















