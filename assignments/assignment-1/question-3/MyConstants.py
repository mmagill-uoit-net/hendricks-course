
import numpy as np
import random


k_B = 1.38e-23


# Boltzmann PDF as a function of each velocity component
def BoltzmannVelocityPDF(v,m=1,T=1):

    A = np.sqrt( m/(2*np.pi*k_B*T) )
    B = np.exp( - (m*v**2)/(2*k_B*T) )

    return A * B

def DrawFrom_BoltzmannVelocityPDF(m=1,T=1):

    sigma = np.sqrt(k_B*T/m)

    return random.gauss(0,sigma)
    








