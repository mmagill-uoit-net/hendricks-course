#ifndef __PROBABILITY_DISTRIBUTION_TYPES_H__
#define __PROBABILITY_DISTRIBUTION_TYPES_H__



#include <stdlib.h>

// Deal with PDFs, CDFs, and iCDFs as types
typedef double (*PDF)(double);
typedef double (*CDF)(double);
typedef double (*ICDF)(double);

// A class for distributions, which stores PDF, CDF, and range
// PDF, CDF, and iCDF must be specified separately at this point
// Could derive CDF and iCDF from PDF, but analytical is still feasible
typedef struct {
  // Range information
  double xmin, xmax;

  // Actual distribution functions
  PDF pdf;
  CDF cdf;
  ICDF icdf;
} Distribution;
void InitializeDistribution( Distribution* dist, 
			     PDF pdf, CDF cdf, ICDF icdf,
			     double xmin, double xmax );


// Functions for distributions
double SampleDistributionUsingITS( Distribution* dist );

// To do (maybe)
/*
void fPrintPDF( FILE* fout, Distribution* dist, int Nbins );
void fPrintCDF( FILE* fout, Distribution* dist, int Nbins );
void fPrintICDF( FILE* fout, Distribution* dist, int Nbins );
*/



#endif /* __PROBABILITY_DISTRIBUTION_TYPES_H__ */
