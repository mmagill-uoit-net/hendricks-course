#include "probability-distribution-types.h"



void InitializeDistribution( Distribution* dist,
			     PDF pdf, CDF cdf, ICDF icdf,
			     double xmin, double xmax ) {

  // Range over which the distribution is defined
  dist->xmin = xmin;
  dist->xmax = xmax;

  // PDF, CDF, and iCDF
  dist->pdf = pdf;
  dist->cdf = cdf;
  dist->icdf = icdf;

}


double SampleDistributionUsingITS( Distribution* dist ) {

  // Sample the unit uniform PDF
  double SampledFrom_UnitUniform = (double) rand() / RAND_MAX;

  // Invert the CDF
  double SampledFrom_Distribution = dist->icdf(SampledFrom_UnitUniform);

  // Return the new sample
  return SampledFrom_Distribution;

}








