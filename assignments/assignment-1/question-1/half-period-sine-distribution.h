#ifndef __HALF_PERIOD_SINE_DISTRIBUTION_H__
#define __HALF_PERIOD_SINE_DISTRIBUTION_H__



#include <math.h>

// PDF, CDF, and iCDF
// All derived analytically because this is easier and more accurate
// For more advanced PDFs, numerical methods might be required
double HalfPeriodSine_PDF( double x );
double HalfPeriodSine_CDF( double x );
double HalfPeriodSine_ICDF( double y );



#endif /* __HALF_PERIOD_SINE_DISTRIBUTION_H__ */


