#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "probability-distribution-types.h"
#include "half-period-sine-distribution.h"



int main (int argc, char* argv[]) {
  // Take in one command line argument, which must be Nsamples
  if ( argc != 2 ) {
    printf("./part-a Nsamples\n");
    return 1;
  }
  char* stopped;
  long Nsamples = strtol(argv[1], &stopped, 10);
  if (*stopped || Nsamples <= 0) {
    printf("Nsamples must be a positive integer!");
    return 1;
  }

  // Make the chosen distribution
  double xmin = 0;
  double xmax = M_PI;
  PDF pdf = &HalfPeriodSine_PDF;
  CDF cdf = &HalfPeriodSine_CDF;
  ICDF icdf = &HalfPeriodSine_ICDF;
  Distribution* dist = malloc(sizeof(Distribution));
  InitializeDistribution(dist,pdf,cdf,icdf,xmin,xmax);

  // Fill an array of samples 
  double* samples = malloc(Nsamples * sizeof(double));
  for ( int i=0; i < Nsamples; i++ ) {
    samples[i] = SampleDistributionUsingITS(dist);
  }

  // Print array to file
  char foutname[256];
  sprintf(foutname,"Nsamples_%06lu.dat",Nsamples);
  FILE* fout = fopen(foutname,"w");
  for ( int i=0; i < Nsamples; i++ ) {
    fprintf(fout,"%.6f\n",samples[i]);
  }
  fclose(fout);
  
  // Wrap up
  free(samples);
  return 0;
}
