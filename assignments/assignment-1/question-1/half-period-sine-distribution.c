#include "half-period-sine-distribution.h"



double HalfPeriodSine_PDF( double x ) {

  if ( x < 0 ) {
    return 0;
  } 
  else if ( x > M_PI ) {
    return 0;
  }
  else {
    return 0.5*sin(x);
  }

}


double HalfPeriodSine_CDF( double x ) {

  if ( x < 0 ) {
    return 0;
  } 
  else if ( x > M_PI ) {
    return 0;
  }
  else {
    return 0.5*(1-cos(x));
  }

}


double HalfPeriodSine_ICDF( double y ) {

  if ( y < 0 ) {
    return -1;
  } 
  else if ( y > 1 ) {
    return -1;
  }
  else {
    return acos(1-2.*y);
  }

}


