import numpy as np
import matplotlib.pyplot as plt


import matplotlib
matplotlib.rcParams.update({'font.size': 20})


def CalcHistogram(filename):

    # These are constants for the current problem
    xmin = 0
    xmax = np.pi

    # Nbins is 20% of Nsamples
    Nsamples = int(filename.split("_")[1].split(".")[0])
    Nbins = 100

    with open(filename,'r') as fin:

        data = np.loadtxt(fin)
        hist = np.histogram(data, bins=np.linspace(xmin,xmax,Nbins))

    return hist


def PlotHistogram(hist,TheColor):

    Range = hist[1][-1] - hist[1][0]
    Nbins = hist[1].shape[0]
    TheWidth = Range / Nbins
    plt.bar(hist[1][:-1],hist[0],width=TheWidth,color=TheColor)
    plt.xlim([0,np.pi])
#    plt.show()



def PlotByNsamples(Nsamples,TheColor):
    
    casename = 'Nsamples_%06d.dat' % Nsamples
    hist = CalcHistogram(casename)
    PlotHistogram(hist,TheColor)


# List of Ns from biggest to smallest,
# so that plots overlap nicely
Ns = [100000,75000,50000,25000,10000]

# Color for each plot
Colors = ['m','g','b','c','r']

# 
x = np.linspace(0,np.pi,100)
dx = x[1] - x[0]
for i in range(len(Ns)):

    ## Unnormalized comparison
    plt.subplot(1,2,1)

    # Plot histogram of samples
    PlotByNsamples(Ns[i],Colors[i])

    # Plot expected distribution
    y = 0.5*np.sin(x) * Ns[i] * dx
    plt.plot(x,y,'k--',linewidth=3)

plt.xlabel('x')
plt.ylabel('N(x)')


## Normalized comparison, low sample rate
plt.subplot(2,2,2)

# Plot 10k histogram, but normalized
hist = CalcHistogram('Nsamples_010000.dat')
Range = hist[1][-1] - hist[1][0]
Nbins = hist[1].shape[0]
TheWidth = Range / Nbins
plt.bar(hist[1][:-1],hist[0]/10000.,width=TheWidth,color='m')
plt.xlim([0,np.pi])
y = 0.5*np.sin(x) * dx
plt.plot(x,y,'k--',linewidth=3)
plt.xlabel('x')
plt.ylabel('rho(x)')
plt.text(0.3,0.016,'10k Samples')


## Normalized comparison, high sample rate
plt.subplot(2,2,4)

# Plot 100k histogram, but normalized
hist = CalcHistogram('Nsamples_100000.dat')
Range = hist[1][-1] - hist[1][0]
Nbins = hist[1].shape[0]
TheWidth = Range / Nbins
plt.bar(hist[1][:-1],hist[0]/100000.,width=TheWidth,color='m')
plt.xlim([0,np.pi])
y = 0.5*np.sin(x) * dx
plt.plot(x,y,'k--',linewidth=3)
plt.xlabel('x')
plt.ylabel('rho(x)')
plt.text(0.3,0.016,'100k Samples')



##
plt.show()










def PlotPDFandCDF():

    x = np.linspace(0,np.pi,100)
    dx = x[1] - x[0]
    
    plt.plot(x,0.5*np.sin(x),
             x,0.5*(1-np.cos(x)))

    plt.xlim([0,np.pi])
    plt.ylim([0,1])
    plt.xlabel('x')

    plt.text(0.6,0.5,'rho(x) = 0.5*sin(x)',color='b')
    plt.text(2.2,0.7,'P(x) = 0.5*(1-cos(x))',color='g')

    plt.show()

