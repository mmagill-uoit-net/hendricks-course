import numpy as np
import matplotlib.pyplot as plt


# Select case
L=100
ICfrac=0.0001

# Load data file
A = np.loadtxt('data/GHCA_%d_%.0e.dat' % (L,ICfrac))

# Count number of timesteps
tmax = A.shape[0]/L;

# Resize by timestep
A = np.resize(A,[tmax,L,L])

# Plot each timestep
for t in range(0,tmax):

    plt.matshow(A[t], cmap=plt.cm.hot, vmin=0, vmax=2)
    plt.savefig('plots/GHCA_%d_%.0e_%d.png' % (L,ICfrac,t))
    plt.close()

