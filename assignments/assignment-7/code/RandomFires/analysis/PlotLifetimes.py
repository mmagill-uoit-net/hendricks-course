import numpy as np
import matplotlib.pyplot as plt


a = np.loadtxt('Lifetime_vs_ICfrac_RandomFires.dat')


myerr = (a[:,2] - a[:,1]**2.)**0.5

plt.xscale('log')

plt.errorbar(a[:,0],a[:,1],yerr=myerr)

plt.xlim([8e-5,1.2])

plt.xlabel('Initial Fire Density')
plt.ylabel('Average Lifetime (Standard Deviation)')

plt.grid(which='both')
plt.show()
