cat frac_1e0.dat | awk -v N=$(wc frac_1e0.dat -l | awk '{print $1}') '{print $1/N}' | paste -sd+ | bc 


cat frac_1e-4.dat | grep -v '^1$' | awk -v N=$(cat frac_1e-4.dat | grep -v '^1$' | wc -l | awk '{print $1}') '{print $1/N}' | paste -sd+ | bc
