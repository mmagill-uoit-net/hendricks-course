#!/bin/bash



for i in {1..50}
do

    ./GHCA.exe 1 >> analysis/frac_1e0.dat
    ./GHCA.exe 0.1 >> analysis/frac_1e-1.dat
    ./GHCA.exe 0.01 >> analysis/frac_1e-2.dat
    ./GHCA.exe 0.001 >> analysis/frac_1e-3.dat
    ./GHCA.exe 0.0001 >> analysis/frac_1e-4.dat


done
