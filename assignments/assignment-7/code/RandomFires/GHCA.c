#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

// The Greenberg–Hastings cellular automaton
// Essentially a model of a forest fire
// State 0 is forested
// State 1 is on fire
// State 2 is burned


int main(int argc, char* argv[]) {

  // Seed by time
  int seed = time(NULL);
  srand( time(NULL) );

  // Domain size (2D)
  int L = 100;

  // Running time
  int tmax = 150;

  // Initial fraction of fire (probabilistic)
  float ICfrac = strtof(argv[1],NULL);

  // Current and old state array
  int* state = malloc(sizeof(float)*L*L);
  int* pstate = malloc(sizeof(float)*L*L);

  // Output file
  FILE* fout;
  char filename[256];
  sprintf(filename,"data/GHCA_%d_%.0e.dat",L,ICfrac);
  fout = fopen(filename,"w");

  // Initial conditions
  float rnd;
  for (int k=0; k<L*L; k++) {
    rnd = (float)rand()/RAND_MAX;
    if (rnd>ICfrac) {
      state[k]=0;
    } else {
      state[k]=1;
    }
  }


  // Main loop
  int index; // 2D array index
  int left,right,up,down; // L,R,U,D indices
  int StillActive=1;
  int t=0;
  while (StillActive) {
    t++;

    // Stop running when all fires are out
    StillActive=0;

    // Copy old state, print to file
    for (int i=0; i<L; i++) {
      for (int j=0; j<L; j++) {
	index = i*L + j;
	pstate[index]=state[index];
	fprintf(fout,"%i ",state[index]);
      }
      fprintf(fout,"\n");
    }
    fprintf(fout,"\n");

    // Iterate
    for (int i=0; i<L; i++) {
      for (int j=0; j<L; j++) {
	index = i*L + j;

	// Periodic BCs (2D Indices)
	if (i!=0) { down = (i-1)*L+j; }      else { down = (L-1)*L+j; }
	if (i!=(L-1)) { up = (i+1)*L+j; }    else { up = 0*i+j; }
	if (j!=0) { left = i*L+(j-1); }      else { left = i*L+(L-1); }
	if (j!=(L-1)) { right = i*L+(j+1); } else { right = i*L+0; }

	// If currently forested,
	if ( pstate[index] == 0 ) {
	  // If any neigbour is on fire, catch on fire, otherwise stay forested
	  if ( (pstate[down]==1) || (pstate[up]==1) || (pstate[left]==1) || (pstate[right]==1) ) {
	    state[index] = 1;
	  }
	  else {
	    state[index] = 0;
	  }
	}

	// If currently on fire, become burned
	if ( pstate[index] == 1 ) { state[index] = 2; StillActive=1; }

	// If currenly burned, become forested
	if ( pstate[index] == 2 ) { state[index] = 0; StillActive=1; }


      }
    }


  }


  printf("%d\n",t);
  return 0;
}


