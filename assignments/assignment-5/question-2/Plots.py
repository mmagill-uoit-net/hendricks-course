import numpy as np
import matplotlib.pyplot as plt





Concs = np.loadtxt('data/30_5_100._10_2_0.5_concentrations.dat')


plt.plot(Concs[:,0],label='Short Rods')
plt.plot(Concs[:,1],label='Long Rods')

plt.xlabel('Time')
plt.ylabel('Number of Rods in the Filtrate')

plt.legend(loc='upper left')
plt.grid()
plt.show()


