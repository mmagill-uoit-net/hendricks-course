import numpy as np
import matplotlib.pyplot as plt
import glob




names = glob.glob("data/*")


for name in names:

    times = np.loadtxt(name)

#    plt.semilogy(times)
    plt.loglog(times)

plt.xlabel('Nth Rod to Cross Membrane')
plt.ylabel('Time to Cross') 

plt.grid()
plt.show()




