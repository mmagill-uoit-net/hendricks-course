# Example input:
# ~/espresso-2.1.2j/Espresso virus_porous_membrane.tcl 5. 5. 100. 10. 2. 0.5

# Parse input (currently cis-trans symmetric and wall thickness of 1 sig)
set Nrod      [lindex $argv 0]
set Nperrod   [lindex $argv 1]
set stiffness [lindex $argv 2]
set Nwallseg  [lindex $argv 3]
set Ngap      [lindex $argv 4]
set F         [lindex $argv 5]
#set rseed        [lindex $argv 7]
#t_random seed    $rseed


########################################################################################

# Function runs a single translocation
global VMD_IS_ON 
set VMD_IS_ON 0
proc RunMain { Nrod Nperrod stiffness Nwallseg Ngap F } {
    global VMD_IS_ON 
    set vis_flag 0

    # Output files
    set CaseName "${Nrod}_${Nperrod}_${stiffness}_${Nwallseg}_${Ngap}_${F}"
    set Ftimes   [open "data/${CaseName}_times.dat"   "w"]

    # Scales
    set sig 1.0
    set eps 1.0

    # Geometry of the domain
    set ywall  50.
    set L     100.

    # Thermostat Parameters
    set temp 1.
    set gamma 1.

    # FENE Potential Parameters
    set kap [expr {30.0*$eps/($sig*$sig)}]
    set lam [expr {1.5*$sig}]

    # LJ Potential Parameters
    set cut   [expr {pow(2.0,1.0/6.0)*$sig}]
    set shift [expr {0.25*$eps}]


    ##########################################################################


    # Spatial domain creation
    setmd box_l $L $L $L

    # Temporal domain creation
    setmd time_step 0.01
    set teq 1000
    set tmax 10000
    setmd skin 0.4

    # Interaction creations
    inter 0 fene $kap $lam
    inter 7 angle $stiffness 3.14159
    inter 0 0  lennard-jones $eps $sig $cut $shift 0.
    inter 0 76 lennard-jones $eps $sig $cut $shift 0.

    # Create solid walls along the boundaries parallel to the membrane
    constraint plane cell  -1      1        -1 type 76
    constraint plane cell  -1 [expr {$L-1}] -1 type 76


    # Create Nrod rods of length Nperrod
    set npart 0
    for { set i 0 } { $i < $Nrod } { incr i } {
	set x [expr {$i*3 + 5.0}]
	for { set j 0 } { $j < $Nperrod } { incr j } {
	    set y [expr {$j*$sig + 5.0}]

	    # Set particle
	    part $npart pos $x $y 0 type 0 fix 0 0 1

	    # FENE Bond
	    if { $j > 0 } {
		part $npart bond 0 [expr {$npart - 1}]
	    }

	    # Bond Angle
	    if { $j > 1 } {
		part [expr {$npart - 1}] bond 7 [expr {$npart - 2}] $npart
	    }

	    # Finished with this particle
	    incr npart

	}
    }

    # Create the wall
    set npartwall 0
    set x 0
    while { $x < $L } {
	# Put down Nwallseg particles
	for { set i 0 } { $i < $Nwallseg } { incr i } {
	    if { $x < $L } {
		part [expr {$npart + $npartwall}] pos $x $ywall 0 fix
		incr npartwall
		set x [expr {$x + $sig}]
	    }
	}
	# Jump to the next segment
	set x [expr {$x + $Ngap}]
    }


    ##########################################################################


    # Initialize Visualization
    if { $vis_flag == 1 } {
	if { $VMD_IS_ON == 0 } {
	    prepare_vmd_connection vmdout
	    imd listen 100
	    set VMD_IS_ON 1
	}
	imd positions
    }


    # Equilibrate, forcing them to stay on the cis side
    constraint plane cell  -1 [expr {$ywall-2}] -1 type 76
    thermostat langevin $temp $gamma
    for { set t 0 } { $t < $teq } { incr t } {
	# Integrate 
	integrate 100
	if { $vis_flag == 1 } {
	    imd positions
	}
    }
    constraint delete 2

    # Apply force field
    for { set i 0 } { $i < $npart } { incr i } {
	part $i ext_force 0 $F 0
    }

    # Simulate
    set t 0
    set nfinished 0
    thermostat langevin $temp $gamma
    while { $nfinished < $Nrod } {

	# When rods are completely above y=ywall, terminate them
	set npart 0
	set nfinished 0
	for { set i 0 } { $i < $Nrod } { incr i } {

	    # Get position of ends
	    set yA [lindex [part               $npart         print folded_position] 1]
	    set yB [lindex [part [expr {int($npart+$Nperrod-1)}] print folded_position] 1]

	    # Decide to freeze rod if it is on the far side of the wall
	    # Move particles to z=1 after they are done to know they are finished
	    if { ($yA>[expr $ywall+$sig]) && ($yB>[expr $ywall+$sig]) } {

		set k [expr {int($i*$Nperrod)}]
		set z [lindex [part $k print folded_position] 2]
		if { $z == 0 } {
		    puts $Ftimes "$t"
		    for { set j 0 } { $j < $Nperrod } { incr j } {
			set k [expr {int($i*$Nperrod + $j)}]
			set x [lindex [part $k print folded_position] 0]
			set y [lindex [part $k print folded_position] 1]
			part $k pos $x $y 1 fix 1 1 1
		    }
		}
		incr nfinished
	    }
	    
	    # Increment rod
	    set npart [expr {int($npart + $Nperrod)}]
	}
	

	# Integrate 
	integrate 100
	if { $vis_flag == 1 } {
	    imd positions
	}
	incr t
    }
    

    close $Ftimes
    return 0
}


##########################################################################
##########################################################################

# Actually run the program

RunMain $Nrod $Nperrod $stiffness $Nwallseg $Ngap $F


