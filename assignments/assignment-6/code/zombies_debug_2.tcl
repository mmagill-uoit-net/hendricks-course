
# Command line
# ./espresso [].tcl N obst_flag rseed

###

## Input parameters

set visu 0

# Parse command line
set N                           [lindex $argv 0]
set obst_flag                   [lindex $argv 1]
set rseed                       [lindex $argv 2]

# Was going to vary these, but not enough time
set zombiesight_over_humansight 1.5
set pwin                        0.25
set pwininfect                  0.25
set Fflee_over_Fzombie          2.0

###

## Parameters that follow from the input

set rseezombie 10.0
set rseehuman [expr {$zombiesight_over_humansight*$rseezombie}]
set rfight [expr {$rseezombie*0.5}]
set rinfect 2.0
set Fzombie 10.0
set Fflee [expr {$Fzombie*$Fflee_over_Fzombie}]

## Output file
set CaseName "${N}_${zombiesight_over_humansight}_${pwin}_${pwininfect}_${Fflee_over_Fzombie}_${obst_flag}_${rseed}"
puts "$CaseName"
set ftally [open "data/${CaseName}_tally.dat"   "w"]


###


t_random seed $rseed
set first [expr srand($rseed)]

set PI 3.14159

set boxL 200
set cx [expr $boxL/2.0]
set cy [expr $boxL/2.0]

set eps 1.0
set sig 1.0
set rc [expr pow(2.0,1.0/6.0)*$sig]
set shi 0.25
set kap [expr 30.0*$eps/($sig*$sig)]
set lam [expr 1.5*$sig]

setmd box_l $boxL $boxL $boxL
setmd periodic 1 1 1
setmd time_step 0.01
setmd skin 0.4

thermostat langevin 1.0 1.0

constraint wall normal 1 0 0  dist 0.001 type 75
constraint wall normal -1 0 0  dist [expr -($boxL-0.001)] type 75
constraint wall normal 0 1 0  dist 0.001 type 75
constraint wall normal 0 -1 0  dist [expr -($boxL-0.001)] type 75


inter 0 75 lennard-jones $eps $sig $rc $shi 0.0
inter 1 75 lennard-jones $eps $sig $rc $shi 0.0
inter 0 76 lennard-jones $eps [expr 4*$sig] [expr {4*$rc}] $shi 0.0
inter 1 76 lennard-jones $eps [expr 4*$sig] [expr {4*$rc}] $shi 0.0


# People
for { set i 0} { $i < $N } {incr i} {
	set theta [expr rand()*2.0*$PI]
	set r [expr rand()*90]
	part $i pos [expr $cx+$r*cos($theta)] [expr $cy+$r*sin($theta)] 0 type 0 fix 0 0 1
	
	set running($i) 0
}

# Obstacles - fixed - z offset for colouring
set npart $N
for { set x 10 } { $x < $boxL } { incr x 20 } {

    for { set y 10 } { $y < $boxL } { incr y 20 } {

	# Build a table
	if { $obst_flag == 1 } {
	    for { set i 0 } { $i < 10 } { incr i } {
		part $npart pos [expr {$x+$i}] $y 0 type 76 fix
		incr npart
		part $npart pos [expr {$x+$i}] $y 1 type 76 fix
		incr npart
	    }
	}

    }

}


# Zombies
set x [lindex [part 0 print pos] 0]
set y [lindex [part 0 print pos] 1]

part 0 pos $cx $cy 1 type 1
part 1 pos $cx $cy 1 type 1

if [expr $visu == 1] {
	prepare_vmd_connection nano 10000
	imd positions
	imd listen 500
}

# Equilibrate
inter ljforcecap 5.
imd positions
integrate 1000
inter ljforcecap 10.
imd positions
integrate 1000
inter ljforcecap 15.
imd positions
integrate 1000
imd positions
inter ljforcecap 0


# Count tallies
set ndead 0
set nzombies 1
set npeople [expr {$N - $nzombies}]
for { set t 0} { $ndead < $N & $nzombies > 0 & $npeople > 0 } {incr t} {

    # Update progress
    if { [expr {fmod($t, 100)}] == 0 } {
	puts "$t: $npeople alive, $nzombies zombies, $ndead dead"
    }

    # Count people each time - count dead cummulative - implies zombies
    set npeople  0
    
    for { set i 0} { $i < $N } {incr i} {
	
	set ptype [part $i print type]

	set x [lindex [part $i print folded_pos] 0]
	set y [lindex [part $i print folded_pos] 1]
	
	set r [expr sqrt( pow($x-$cx,2.0) + pow($y-$cy,2.0) ) ]
	
	#People
	if { $ptype == 0 } { 
	    
	    incr npeople

	    part $i type 7
	    set rtozombie [analyze mindist 7 1]
	    #NEW NEW
	    part $i type 0
	    
	    if { $rtozombie < $rseezombie } {
		#NEW NEW				
		set neighlist [analyze nbhood $i [expr $rtozombie+0.01] ]
		set nneigh [llength $neighlist]
		for { set j 0 } { $j < $nneigh } { incr j } {
		    set pindex [lindex $neighlist $j]
		    set ptype [part $pindex print type]
		    if { $ptype == 1 } {
			set zombieid $pindex
		    }
		}
		
		
		if { $rtozombie > $rfight } { 		#FLEE
		    part $i type 0
		    set zomx [lindex [part $zombieid print folded_pos] 0]
		    set zomy [lindex [part $zombieid print folded_pos] 1]
		    set dx [expr $zomx-$x]
		    set dy [expr $zomy-$y]
		    set dr [expr sqrt($dx*$dx + $dy*$dy)]
		    
		    set Fx [expr -$Fflee*$dx/$dr]
		    set Fy [expr -$Fflee*$dy/$dr]
		    
		    part $i ext_force $Fx $Fy 0
		    
		} else {			#FIGHT
		    part $i pos $x $y 4
		    set rnd [expr rand()]
		    if { $rnd < $pwin } {	#WIN
			#deactivate zombie
			set zombx [lindex [part $zombieid print folded_pos] 0]
			set zomby [lindex [part $zombieid print folded_pos] 1]
			part $zombieid pos $zombx $zomby 2 type 10 fix
			incr ndead
			
			set rnd [expr rand()]
			if { $rnd < $pwininfect } { #BUT INFECTED
			    part $i type 1
			    part $i pos $x $y 1
			    part $i ext_force 0 0 0
			} else {
			    part $i type 0
			    part $i pos $x $y 0
			}
		    } else {		#LOSE
			part $i type 1
			part $i pos $x $y 1
			part $i ext_force 0 0 0
		    }
		}
		
	    } else {
		
		part $i ext_force 0 0 0
		
		part $i type 0
		part $i pos $x $y 0
		
	    }
	} elseif { $ptype == 1 } {	#ZOMBIES
	    
	    part $i ext_force 0 0 0
	    
	    part $i type 8
	    set rtohuman [analyze mindist 8 0]
	    part $i type 1
	    
	    if { $rtohuman < $rseehuman } {
		#NEW NEW				
		set neighlist [analyze nbhood $i [expr $rtohuman+0.01] ]
		set nneigh [llength $neighlist ]
		for { set j 0 } { $j < $nneigh } { incr j } {
		    set pindex [lindex $neighlist $j]
		    set ptype [part $pindex print type]
		    flush stdout
		    if { $ptype == 0 } {
			set humanid $pindex
		    }
		}
		if { $rtohuman < $rinfect } {	#CLOSE - INFECT
		    part $humanid pos $x $y 1 type 1
		    set running($humanid) 0
		    part $humanid ext_force 0 0 0
		} else {			#CHASE
		    
		    set humx [lindex [part $humanid print folded_pos] 0]
		    set humy [lindex [part $humanid print folded_pos] 1]
		    set dx [expr $humx-$x]
		    set dy [expr $humy-$y]
		    set dr [expr sqrt($dx*$dx + $dy*$dy)]
		    
		    set Fx [expr $Fzombie*$dx/$dr]
		    set Fy [expr $Fzombie*$dy/$dr]
		    
		    part $i ext_force $Fx $Fy 0
		}
	    }
	}
	
    }

    integrate 5
    imd positions

    # Output counts
    set nzombies [expr {$N - $npeople - $ndead}]
    puts $ftally "$npeople $nzombies $ndead"
    
}
close $ftally
