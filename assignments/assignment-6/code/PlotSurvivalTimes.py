import numpy as np
import matplotlib.pyplot as plt
import glob


Ns = [100, 150, 200, 250, 300]

avg_without = np.zeros([len(Ns),])
avg_with = np.zeros([len(Ns),])
avg2_without = np.zeros([len(Ns),])
avg2_with = np.zeros([len(Ns),])
nvictories_without = np.zeros([len(Ns),])
nvictories_with = np.zeros([len(Ns),])


for k in range(len(Ns)):
    # For each N case
    N = Ns[k]

    ###
    for obst_flag in [0,1]:

        # Sum the survival times
        total = 0
        total2 = 0
        nvictories = 0
        for rseed in range(1,21):
            # For each random seed
            
            # Count the number of lines (equal number of timesteps)
            filename = 'data/%d_1.5_0.25_0.25_2.0_%d_%d_tally.dat' % (N,obst_flag,rseed)
            with open(filename) as f:
                for i, l in enumerate(f):
                    pass
                    linecount = i + 1

            # Add to total if all humans died, else count
            if ( int(l.split()[0]) > 0 ):
                # Humans won
                nvictories += 1
            else:
                total = total + linecount
                total2 = total2 + (linecount**2)

        # Compute the average survival time
        print (N,obst_flag,nvictories)
        if ( nvictories < 20 ):
            if ( obst_flag == 0 ):
                avg_without[k] = total / (20. - nvictories)
                avg2_without[k] = total2 / (20. - nvictories)
                nvictories_without[k] = nvictories
            else:
                avg_with[k] = total / (20. - nvictories)
                avg2_with[k] = total2 / (20. - nvictories)
                nvictories_with[k] = nvictories

# Errors
std_avg_without = (avg2_without - avg_without**2)**(0.5)
std_avg_with = (avg2_with - avg_with**2)**(0.5)
stderr_avg_without = np.divide(std_avg_without, (20. - nvictories_without)**0.5)
stderr_avg_with = np.divide(std_avg_with, (20. - nvictories_with)**0.5)

# nVictories is Poisson distributed, so std = sqrt(mean) 
std_nvictories_without = nvictories_without**0.5
std_nvictories_with = nvictories_with**0.5

# Plot
plt.subplot(2,1,1)
plt.errorbar(Ns,avg_without,label='No Obstacles w/ Std Err',yerr=stderr_avg_without) 
plt.errorbar(Ns,avg_with,label='With Obstacles w/ Std Err',yerr=stderr_avg_with) 

plt.xlabel('Initial Number of People')
plt.ylabel('Average Survival Time')
plt.xlim([90,310])
plt.legend()
plt.grid()

plt.subplot(2,1,2)
plt.errorbar(Ns,nvictories_without,label='No Obstacles w/ Std Dev',yerr=std_nvictories_without) 
plt.errorbar(Ns,nvictories_with,label='With Obstacles w/ Std Dev',yerr=std_nvictories_with) 

plt.xlabel('Initial Number of People')
plt.ylabel('Victories')
plt.xlim([90,310])
plt.ylim([0,20])
plt.legend(loc='upper left')
plt.grid()

plt.show()
