import numpy as np
import matplotlib.pyplot as plt
import glob


Ns = [100, 150, 200, 250, 300]

for N in Ns:
    # For each N case

    for obst_flag in [0,1]:
        # For with and without obstacles

        for rseed in range(1,21):
            # For each random seed

            # Turn the file into a matrix
            casename = '....'
            curdata = np.loadtxt(casename)

            # Plot to the scatter plot with the correct color
            plt.scatter(curdata[1],curdata[2])



plt.xlabel('Number of Zombies')
plt.ylabel('Number of Dead')
plt.grid()

plt.show()

